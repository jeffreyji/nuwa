package com.nuwa.reactive.router;

import com.nuwa.reactive.handler.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:24
 */
@Configuration(proxyBeanMethods = false)
public class UserRouter {
    @Bean
    public RouterFunction<ServerResponse> route(UserHandler userHandler) {

        return RouterFunctions
                .route(GET("/createUser2").and(accept(MediaType.APPLICATION_JSON)), userHandler::getUserById);
    }
}
