package com.nuwa.reactive.handler;

import com.nuwa.reactive.domain.User;
import com.nuwa.reactive.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:26
 */
@Component
@Slf4j
public class UserHandler {
    @Autowired
    private UserRepository userRepository;

    public Mono<ServerResponse> getUserById(ServerRequest request) {
        ServerHttpRequest req = request.exchange().getRequest();
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(userRepository.findAll(), User.class);
    }
}
