//package com.nuwa.reactive.service.impl;
//
//import com.nuwa.reactive.domain.User;
//import com.nuwa.reactive.repository.UserRepository;
//import com.nuwa.reactive.service.IUserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import reactor.core.publisher.Mono;
//
//import java.util.concurrent.TimeUnit;
//
///**
// * @description:
// * @author: jijunhui
// * @date:2022/3/14 19:45
// */
//@Service
//public class UserServiceImpl implements IUserService {
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @Override
//    public Mono<User> getById(Long id) {
//        return userRepository.findById(id);
//    }
//}
