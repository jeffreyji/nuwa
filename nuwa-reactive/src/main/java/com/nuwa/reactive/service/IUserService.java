package com.nuwa.reactive.service;

import com.nuwa.reactive.domain.User;
import reactor.core.publisher.Mono;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:45
 */
public interface IUserService {
    /**
     * 根据id获取用户信息
     * @param id
     * @return
     */
    Mono<User> getById(Long id);
}
