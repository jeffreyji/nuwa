package com.nuwa.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:20
 */
@SpringBootApplication
@EnableWebFlux
public class ReactiveApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ReactiveApplication.class, args);
        //GreetingClient greetingClient = context.getBean(GreetingClient.class);
        //// We need to block for the content here or the JVM might exit before the message is logged
        //System.out.println(">> message = " + greetingClient.getMessage().block());
    }
}
