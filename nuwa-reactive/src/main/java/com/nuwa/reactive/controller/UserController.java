package com.nuwa.reactive.controller;

import com.nuwa.reactive.domain.User;
import com.nuwa.reactive.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:52
 */
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping("create")
    public Mono<User> createUser(@RequestBody User user) {
        return userRepository.insert(user);
    }

    @RequestMapping("modify")
    public Mono<User> modifyUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    @RequestMapping("delete/{id}")
    public Mono<Void> deleteUser(@PathVariable String id) {
        return userRepository.deleteById(id);
    }

    @RequestMapping("/{id}")
    public Mono<User> getUserById(@PathVariable String id) {
        return userRepository.findById(id);
    }

    @RequestMapping("/getAll")
    public Flux<User> getAllUsers() {
        return userRepository.findAll();
    }

    @RequestMapping(value = "/getAll2",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<User> getAllUsers2() {
        return userRepository.findAll();
    }
}
