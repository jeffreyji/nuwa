package com.nuwa.reactive.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/14 19:25
 */
@Data
@Document(collection = "user")
public class User implements Serializable {

    @Id
    private String id;
    private String name;
    private Integer age;

    public User(){}

    public User(String _name,Integer _age){
        this.name = _name;
        this.age = _age;
    }
}
