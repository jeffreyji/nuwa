package com.nuwa.demo.constant;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/8/29 上午12:08
 * @version: 1.0
 */
public interface KafkaTopic {
    /**
     * @description: 保存用户
     * @author Jeffrey
     * @date ${DATE} ${TIME}
     * @version 1.0
     */
    String SAVE_USER_TOPIC="nuwa.save.user";
}
