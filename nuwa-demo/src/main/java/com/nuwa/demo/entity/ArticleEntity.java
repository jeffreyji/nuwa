//package com.nuwa.demo.entity;
//
//import lombok.Data;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.elasticsearch.annotations.Document;
//
//import java.io.Serializable;
//
///**
// * @author jijunhui
// * @version 1.0.0
// * @date 2021/2/23 20:24
// * @description todo
// */
///**
// * 文章
// */
//@Document(indexName = "elasticsearch")
//@Data
//public class ArticleEntity implements Serializable {
//    // 作者信息
//    private String writer;
//
//    // 文章信息
//    @Id
//    private Long id;
//
//    private String title;
//
//    private String content;
//
//    // 归属信息
//    private Long admin;
//}
