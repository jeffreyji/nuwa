//package com.nuwa.demo.entity;
//
//import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableLogic;
//import com.baomidou.mybatisplus.annotation.TableName;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.experimental.Accessors;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * <p>
// * 用户表
// * </p>
// *
// * @author jijunhui
// * @since 2020-07-01
// */
//@Data
//@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
//@TableName("TB_DEMO")
//public class Demo implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    /**
//     * 用户ID
//     */
//    @TableField("ID")
//    private Long id;
//
//    @TableField("NAME")
//    private String name;
//
//    /**
//     * 创建时间
//     */
//    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
//    private Date createTime;
//
//    /**
//     * 更新时间
//     */
//    @TableField(value = "UPDATE_TIME", fill = FieldFill.INSERT_UPDATE)
//    private Date updateTime;
//
//    /**
//     * 删除标识 0：正常  1：已删除
//     */
//    @TableLogic
//    @TableField(value = "DEL_FLAG", fill = FieldFill.INSERT)
//    private Integer delFlag;
//
//}
