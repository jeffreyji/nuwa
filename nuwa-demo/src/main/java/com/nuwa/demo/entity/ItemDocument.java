//package com.nuwa.demo.entity;
//
//import lombok.Data;
////import org.springframework.data.annotation.Id;
////import org.springframework.data.elasticsearch.annotations.Document;
////import org.springframework.data.elasticsearch.annotations.Field;
////import org.springframework.data.elasticsearch.annotations.FieldType;
//
///**
// * @author jijunhui
// * @version 1.0.0
// * @date 2021/2/23 20:20
// * @description todo
// */
////@Document(indexName = ItemDocument.INDEX)
//@Data
//public class ItemDocument {
//    public static final String INDEX = "test";
//
//
//    public ItemDocument() {
//    }
//
//    public ItemDocument(Long id, Integer catId, String name, Long price, String description) {
//        this.id = id;
//        this.catId = catId;
//        this.name = name;
//        this.price = price;
//        this.description = description;
//    }
//
//    /**
//     * 商品唯一标识
//     */
////    @Id
////    @Field(type = FieldType.Keyword)
//    private Long id;
//
//    /**
//     * 类目id
//     */
////    @Field(type = FieldType.Integer)
//    private Integer catId;
//
//    /**
//     * 商品名称
//     */
////    @Field(type = FieldType.Text, index = false)
//    private String name;
//
//
//    /**
//     * 商品价格
//     */
////    @Field(type = FieldType.Long)
//    private Long price;
//
//
//    /**
//     * 商品的描述
//     */
////    @Field(type = FieldType.Text, searchAnalyzer = "ik", analyzer = "ik")
//    private String description;
//}
