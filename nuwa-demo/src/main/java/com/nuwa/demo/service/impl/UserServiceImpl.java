package com.nuwa.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.demo.entity.User;
import com.nuwa.demo.mapper.UserMapper;
import com.nuwa.demo.service.IUserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2021-08-28
 */
@Service
@Log4j2
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void saveUser() {
        User user = new User();
        user.setName("hello");
        this.save(user);
        log.info("第一次保存用户信息成功:{}", user);
        eventPublisher.publishEvent(user);
        //int i = 100 / 0;

        user = new User();
        user.setName("world");
        this.save(user);
        log.info("第二次保存用户信息成功:{}", user);
        eventPublisher.publishEvent(user);
        log.info("整个方法结束。。。。。");
    }
}
