//package com.nuwa.demo.service.impl;
//
//import com.baomidou.dynamic.datasource.annotation.DS;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.nuwa.demo.dao.DemoMapper;
//import com.nuwa.demo.entity.Demo;
//import com.nuwa.demo.service.IDemoService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// * <p>
// * 用户表 服务实现类
// * </p>
// *
// * @author jijunhui
// * @since 2020-07-01
// */
//@Service
//@Slf4j
//public class DemoServiceImpl extends ServiceImpl<DemoMapper, Demo> implements IDemoService {
//    @Override
//    public void update0() {
//        update1();
//        update2();
//    }
//
//    @Override
//    @Transactional
//    public void update1() {
//        Demo demo = getById(1);
//        demo.setName("update1");
//        updateById(demo);
//        log.info("更新成功:{}", demo);
//
//    }
//
//    @Override
//    @DS("read")
//    public void update2() {
//        Demo demo = getById(1);
//        demo.setName("read");
//        int n = 10 / 0;
//        updateById(demo);
//        log.info("read更新成功:{}", demo);
//    }
//
//
////    @Override
////    public List<User> selectPage(IPage<User> page, Integer state) {
////        return this.baseMapper.selectPage(page,state);
////    }
//}
