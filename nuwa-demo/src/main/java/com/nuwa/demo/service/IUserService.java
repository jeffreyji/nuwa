package com.nuwa.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuwa.demo.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author jeffrey
 * @since 2021-08-28
 */
public interface IUserService extends IService<User> {
    /**
     * @description: 保存用户
     * @author Jeffrey
     * @date ${DATE} ${TIME}
     * @version 1.0
     */
    void saveUser();

}
