package com.nuwa.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: JeffreyJi
 * @date: 2020/10/11 20:58
 * @version: 1.0
 * @description: demo服务启动类
 */
//@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.nuwa"})
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class);
    }
}
