package com.nuwa.demo.integrate.easyexcel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Created by diaobisong on 2018/10/25.
 */
@Data
public class DemoData {

    /**
     * 订单号
     */
    @ExcelProperty("订单号")
    private String orderNo;
    /**
     * 订单状态
     */
    @ExcelProperty("运单状态")
    private String statusName;
    /**
     * 装货地址
     */
    @ExcelProperty("装货地")
    private String fromAddress;
    /**
     * 卸货地址
     */
    @ExcelProperty("卸货地")
    private String toAddress;
    /**
     * 承运司机名称
     */
    @ExcelProperty("承运司机")
    private String askDriverName;

    @ExcelProperty("司机手机号")
    private String askDriverPhone;
    /**
     * 承运车牌号
     */
    @ExcelProperty("车牌号")
    private String askVehiclePlateNo;
    /**
     * 货物名称
     */
    @ExcelProperty("货物名称")
    private String cargoName;
    /**
     * 总运费（单位元）
     */
    @ExcelProperty("总运费（元）")
    private String totalPriceName;
    /**
     * 预付现金
     */
    @ExcelProperty("预付现金（元）")
    private String prePayCashAmountName;

    /**
     * 预付油卡金额
     */
    @ExcelProperty("预付油卡（元）")
    private String prePayOilAmountName;
    /**
     * 尾款现金
     */
    @ExcelProperty("尾款现金（元）")
    private String restPayCashAmountName;

    /**
     * 尾款油卡金额
     */
    @ExcelProperty("尾款油卡（元）")
    private String restPayOilAmountName;
    /**
     * 回单支付现金金额
     */
    @ExcelProperty("回单现金（元）")
    private String receiptPayCashAmountName;

    /**
     * 回单支付油卡金额
     */
    @ExcelProperty("回单油卡（元）")
    private String receiptPayOilAmountName;
    /**
     * 订单创建时间
     */
    @ExcelProperty("创建时间")
    private String createTimeStr;

    /**
     * 创建人姓名
     */
    @ExcelProperty("创建人")
    private String createByUserName;

    /**
     * 外部订单号
     */
    @ExcelProperty("委托单号")
    private String outerTradeNo;

    @ExcelIgnore
    private Date createTime;

    @ExcelProperty("失败原因")
    private String msg;

}
