package com.nuwa.demo.integrate.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/15 16:53
 */
@Slf4j
public class ReadExcel {
    public final static String FILE_PATH = "D:\\Workspace\\nuwa\\nuwa-demo\\src\\main\\java\\com\\nuwa\\demo\\integrate\\easyexcel\\1650012603106.xls";

    @Test
    public void simpleRead() throws Exception {
        // 写法1：JDK8+ ,不用额外写一个DemoDataListener
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        // 这里每次会读取3000条数据 然后返回过来 直接调用使用数据就行
        EasyExcel.read(FILE_PATH, DemoData.class, new PageReadListener<DemoData>(dataList -> {
            for (DemoData demoData : dataList) {
                log.info("读取到一条数据{}", JSON.toJSONString(demoData));
            }
        })).sheet().doRead();
        System.in.read();

        // 写法2：
        // 匿名内部类 不用额外写一个DemoDataListener
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(FILE_PATH, DemoData.class, new ReadListener<DemoData>() {
            /**
             * 单次缓存的数据量
             */
            public static final int BATCH_COUNT = 100;
            /**
             *临时存储
             */
            private List<DemoData> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

            @Override
            public void invoke(DemoData data, AnalysisContext context) {
                cachedDataList.add(data);
                if (cachedDataList.size() >= BATCH_COUNT) {
                    saveData();
                    // 存储完成清理 list
                    cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
                }
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext context) {
                saveData();
            }

            /**
             * 加上存储数据库
             */
            private void saveData() {
                log.info("{}条数据，开始存储数据库！", cachedDataList.size());
                log.info("存储数据库成功！");
            }
        }).sheet().doRead();

        // 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        // 写法3：
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        //EasyExcel.read(FILE_PATH, DemoData.class, new DemoDataListener()).sheet().doRead();

        // 写法4：
        // 一个文件一个reader
        //ExcelReader excelReader = null;
        //try {
        //    excelReader = EasyExcel.read(FILE_PATH, DemoData.class, new DemoDataListener()).build();
        //    // 构建一个sheet 这里可以指定名字或者no
        //    ReadSheet readSheet = EasyExcel.readSheet(0).build();
        //    // 读取一个sheet
        //    excelReader.read(readSheet);
        //} finally {
        //    if (excelReader != null) {
        //        // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
        //        excelReader.finish();
        //    }
        //}
    }
}
