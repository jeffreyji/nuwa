package com.nuwa.demo.integrate.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.nuwa.demo.integrate.easyexcel.ReadExcel.FILE_PATH;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/15 17:28
 */
@Slf4j
public class WriteExcel {

    public final static String WRITE_PATH = "D:\\Workspace\\nuwa\\nuwa-demo\\src\\main\\java\\com\\nuwa\\demo\\integrate\\easyexcel\\";

    @Test
    public void testSimpleWrite() throws Exception {
        // 写法1 JDK8+
        // since: 3.0.0-beta1
        String fileName = WRITE_PATH + "simpleWrite" + System.currentTimeMillis() + ".xlsx";

        List<DemoData> result = new ArrayList<>();
        EasyExcel.read(FILE_PATH, DemoData.class, new PageReadListener<DemoData>(dataList -> {
            for (DemoData demoData : dataList) {
                log.info("读取到一条数据{}", JSON.toJSONString(demoData));
                demoData.setMsg("这个是错误信息");
                result.add(demoData);
            }
        })).sheet().doRead();

        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, DemoData.class)
                .sheet("模板")
                .doWrite(() -> {
                    // 分页查询数据
                    return result;
                });
    }
}
