package com.nuwa.demo.listen;

import com.nuwa.demo.constant.KafkaTopic;
import com.nuwa.demo.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.concurrent.TimeUnit;

/**
 * @description: 用户事物监听
 * @author: Jeffrey
 * @date: 2021/8/29 上午1:05
 * @version: 1.0
 */
@Slf4j
@Component
public class UserTransactionListener {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @EventListener(classes = {User.class})
    @Order(100)
    public void onApplicationEvent(User user){
        log.info("请求到普通的事件通知:{}",user);
    }

    //    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, classes = User.class)
    @Order(99)
    public void handel(User user) {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("数据库事务执行操作完成:{}", user);
        //kafkaTemplate.send(KafkaTopic.SAVE_USER_TOPIC, user.getId() + "");

    }
}
