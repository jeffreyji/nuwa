package com.nuwa.demo.config;

import com.nuwa.demo.constant.KafkaTopic;
import com.nuwa.demo.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @description: kafka消费类
 * @author: Jeffrey
 * @date: 2021/8/29 上午12:05
 * @version: 1.0
 */
@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    private IUserService userService;

    @KafkaListener(topics = {KafkaTopic.SAVE_USER_TOPIC})
    void listenSaveUser(ConsumerRecord consumerRecord){
        final val key = consumerRecord.key();
        final val value = consumerRecord.value();
        log.info("kafka收到的信息,key={},value:{}",key,value);
        final val user = userService.getById(Long.valueOf(value.toString()));
        log.info("获取到的用户信息是:{}",user);


    }
}
