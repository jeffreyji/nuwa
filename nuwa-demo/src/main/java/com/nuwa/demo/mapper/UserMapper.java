package com.nuwa.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuwa.demo.entity.User;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2021-08-28
 */
public interface UserMapper extends BaseMapper<User> {

}
