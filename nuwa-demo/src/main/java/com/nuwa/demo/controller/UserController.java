package com.nuwa.demo.controller;


import com.nuwa.common.global.dto.GlobalResponseDto;
import com.nuwa.demo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jeffrey
 * @since 2021-08-28
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @RequestMapping("save")
    public GlobalResponseDto saveUser(){
        userService.saveUser();
        return GlobalResponseDto.success();
    }

}
