package com.nuwa.demo.controller;

import com.nuwa.demo.vo.TestVo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author jijunhui
 * @Date 2022/1/25 23:02
 * @Version 1.0.0
 * @Description
 */
@RestController
public class TestController {

    @RequestMapping("test")
    public TestVo test(@RequestBody TestVo testVo) {
        return testVo;
    }
}
