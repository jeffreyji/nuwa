//package com.nuwa.demo.controller;
//
//import com.nuwa.common.global.dto.GlobalResponseDto;
//import com.nuwa.demo.entity.ItemDocument;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
////import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author jijunhui
// * @version 1.0.0
// * @date 2021/2/23 20:18
// * @description todo
// */
//@RestController
//@RequestMapping("es")
//@Slf4j
//public class EsController {
//    @Autowired
////    private ElasticUtils elasticUtils;
//    private ElasticsearchRestTemplate elasticsearchTemplate;
//
//    @GetMapping("save")
//    public GlobalResponseDto<Void> save() {
//        ItemDocument itemDocument = new ItemDocument();
//        itemDocument.setCatId(5);
//        itemDocument.setDescription("这可真是啊棒棒");
//        itemDocument.setId(4L);
//        itemDocument.setName("啊3凯");
//        ItemDocument save = elasticsearchTemplate.save(itemDocument);
//        log.info("{}", save);
//        return GlobalResponseDto.success();
//    }
//
//    @GetMapping("get")
//    public GlobalResponseDto<ItemDocument> get() {
//        ItemDocument itemDocument = elasticsearchTemplate.get("5", ItemDocument.class);
//        log.info("{}", itemDocument);
//        return GlobalResponseDto.success(itemDocument);
//    }
//}
