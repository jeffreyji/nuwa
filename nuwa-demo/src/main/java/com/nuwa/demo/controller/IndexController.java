//package com.nuwa.demo.controller;
//
//import com.nuwa.common.global.dto.GlobalRequestDto;
//import com.nuwa.common.global.dto.GlobalResponseDto;
//import com.nuwa.demo.entity.Demo;
//import com.nuwa.demo.service.IDemoService;
//import com.nuwa.demo.vo.FileVo;
//import com.nuwa.demo.vo.UserVo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Date;
//import java.util.concurrent.ThreadLocalRandom;
//
///**
// * @author jijunhui
// * @version 1.0.0
// * @date 2020/10/16 15:09
// * @description demo测试
// */
//@RestController
//@RequestMapping("user")
//@Slf4j
//public class IndexController {
//    @Autowired
//    private IDemoService demoService;
//
//    @RequestMapping("/index")
//    public UserVo index(@RequestBody UserVo userVo) {
//        int n = 100 / 0;
//        return userVo;
//    }
//
//    @RequestMapping("/upload")
//    public GlobalResponseDto<Void> uploadFile(@RequestBody GlobalRequestDto<FileVo> fileVoGlobalRequestDto) {
//        FileVo fileVo = fileVoGlobalRequestDto.getBody();
//        log.info(fileVo.getFileName());
//        log.info(fileVo.getFileContent());
//        return GlobalResponseDto.success();
//    }
//
//    @GetMapping("insert")
//    public GlobalResponseDto<Demo> insert() {
//        Demo demo = new Demo();
//        demo.setName("name" + ThreadLocalRandom.current().nextInt(100));
//        demo.setCreateTime(new Date());
//        demoService.save(demo);
//        return GlobalResponseDto.success(demo);
//    }
//
//    @GetMapping("get")
//    public GlobalResponseDto<Demo> get() {
//        return GlobalResponseDto.success(demoService.getById(1));
//    }
//
//    @GetMapping("update")
//    public GlobalResponseDto<Demo> update() {
//        demoService.update0();
//        return GlobalResponseDto.success();
//    }
//}
