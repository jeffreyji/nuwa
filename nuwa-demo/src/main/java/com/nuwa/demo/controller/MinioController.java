package com.nuwa.demo.controller;

import com.nuwa.common.oss.util.MinioUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;

@RestController
public class MinioController {

    @Autowired
    private MinioUtil minioUtil;

    @PostMapping("upload")
    public String upload(@RequestPart("files") MultipartFile[] files) throws Exception {
        String bucketName = "test";
        Arrays.stream(files).forEach(file -> {
            try {
                minioUtil.upload(bucketName, file.getName(), file.getInputStream(), file.getSize());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return "success";
    }
}
