//package com.nuwa.demo.controller;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author jijunhui
// * @version 1.0.0
// * @date 2020/11/13 19:29
// * @description nacos web
// */
//@RestController
//@RequestMapping("nacos/config")
//@Slf4j
//@RefreshScope
//public class NacosConfigController {
//
//    @Value("${key1:公共key1}")
//    private String key1;
//    @Value("${key2:公共key2}")
//    private String key2;
//
//    @Value("${demo1:私有key1}")
//    private String demo1;
//
//    @Value("${demo2:私有key2}")
//    private String demo2;
//
//    @RequestMapping("/getAllConfigValue")
//    public String getAllConfigValue() {
//        return key1 + key2 + demo1 + demo2;
//    }
//}
