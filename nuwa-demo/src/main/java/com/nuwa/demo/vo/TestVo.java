package com.nuwa.demo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author jijunhui
 * @Date 2022/1/25 23:03
 * @Version 1.0.0
 * @Description
 */
@Data
public class TestVo implements Serializable {

    private Map<String, String> map = new HashMap<>();
}
