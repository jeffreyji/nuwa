package com.nuwa.demo.vo;

import com.nuwa.common.global.dto.MobileRequest;
import lombok.Data;

/**
 * @Author jijunhui
 * @Date 2021/1/24 14:19
 * @Version 1.0.0
 * @Description
 */
@Data
public class FileVo extends MobileRequest {
    private String fileName;
    private String fileContent;

}
