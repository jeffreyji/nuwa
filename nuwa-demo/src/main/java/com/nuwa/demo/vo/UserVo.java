package com.nuwa.demo.vo;

import com.nuwa.common.global.dto.MobileRequest;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @Author jijunhui
 * @Date 2021/1/23 22:57
 * @Version 1.0.0
 * @Description
 */
@Data
public class UserVo extends MobileRequest {

    @NotEmpty(message = "姓名不能时空")
    private String name;
}
