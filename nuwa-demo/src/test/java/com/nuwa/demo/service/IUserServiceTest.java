package com.nuwa.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/8/29 上午12:16
 * @version: 1.0
 */
@SpringBootTest
@Slf4j
class IUserServiceTest {

    @Autowired
    private IUserService userService;
    @Test
    void saveUser() {
        userService.saveUser();
    }
}