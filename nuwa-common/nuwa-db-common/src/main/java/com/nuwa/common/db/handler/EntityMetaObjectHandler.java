package com.nuwa.common.db.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/29 20:08
 * @description mybatis-plus 自动填充
 */
@Component
@Slf4j
public class EntityMetaObjectHandler implements MetaObjectHandler {
    private final static String DELETED = "DELETED";
    private final static String CREATE_TIME = "CREATE_TIME";
    private final static String UPDATE_TIME = "UPDATE_TIME";
    private final static String CREATE_BY = "CREATE_BY";
    private final static String UPDATE_BY = "UPDATE_BY";

    @Override
    public void insertFill(MetaObject metaObject) {
        final val now = LocalDateTime.now();
        // 起始版本 3.3.3(推荐)
        this.setFieldValByName(CREATE_TIME, now, metaObject);
        this.setFieldValByName(UPDATE_TIME, now, metaObject);
        this.strictInsertFill(metaObject, DELETED, () -> 1, Integer.class);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 起始版本 3.3.3(推荐)
        this.setFieldValByName(UPDATE_TIME, LocalDateTime.now(), metaObject);
        this.setFieldValByName(UPDATE_BY, LocalDateTime.now(), metaObject);
    }
}
