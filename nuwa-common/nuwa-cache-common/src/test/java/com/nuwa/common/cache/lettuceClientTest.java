//package com.nuwa.common.cache;
//
//import io.lettuce.core.RedisClient;
//import io.lettuce.core.RedisURI;
//import io.lettuce.core.api.StatefulRedisConnection;
//import io.lettuce.core.api.sync.RedisCommands;
//import lombok.extern.slf4j.Slf4j;
//import lombok.val;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.time.Duration;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//
//@Slf4j
//public class lettuceClientTest {
//    private static final String LOCALHOST = "127.0.0.1";
//    private static final Integer PORT = 6379;
//
//    private RedisCommands<String, String> redisCommands;
//
//
//    private static ExecutorService executorService = Executors.newScheduledThreadPool(10);
//
//    @Before
//    public void init() {
//        RedisURI redisURI = new RedisURI(LOCALHOST, PORT, Duration.ofSeconds(2));
//        RedisClient redisClient = RedisClient.create(redisURI);
//        StatefulRedisConnection<String, String> connect = redisClient.connect(redisURI);
//        redisCommands = connect.sync();
//    }
//
//    @Test
//    public void test1() throws InterruptedException {
//        for (int i = 0; i < 10; i++) {
//            executorService.submit(() -> {
//                String threadName = Thread.currentThread().getName();
//                log.info("线程:{}开始抢锁", threadName);
//                val v = redisCommands.setnx("lock:test", "v");
//                if (v) {
//                    log.info("线程:{}获取锁成功!", threadName);
//                    try {
//                        TimeUnit.SECONDS.sleep(5);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    log.info("当前线程:{}获取到的结果是:{}", Thread.currentThread().getName(), v);
//                }
//            });
//        }
//        TimeUnit.SECONDS.sleep(100);
//        log.info("程序结束");
//    }
//
//    public static void main(String[] args) {
//        System.out.println(123123123);
//    }
//}
