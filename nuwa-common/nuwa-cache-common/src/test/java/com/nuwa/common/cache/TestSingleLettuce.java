package com.nuwa.common.cache;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;

/**
 * @Author jijunhui
 * @Date 2021/4/11 14:38
 * @Version 1.0.0
 * @Description
 */
@Slf4j
public class TestSingleLettuce {

    public static void main(String[] args) {
        RedisURI redisURI = new RedisURI("8.140.138.107", 6379, Duration.ofSeconds(2));
        RedisClient redisClient = RedisClient.create(redisURI);
        StatefulRedisConnection<String, String> connect = redisClient.connect(redisURI);
        RedisCommands<String, String> redisCommands = connect.sync();
        String set = redisCommands.set("k1", "v1");
        log.info("{}", redisCommands.get("k1"));
        redisCommands.shutdown(true);
        redisClient.shutdown();

    }
}
