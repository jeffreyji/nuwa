//package com.nuwa.common.cache;
//
//import lombok.extern.slf4j.Slf4j;
//import lombok.val;
//import org.junit.Before;
//import org.junit.Test;
//import org.redisson.Redisson;
//import org.redisson.api.RedissonClient;
//import org.redisson.config.Config;
//
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//
//@Slf4j
//public class RedissonClientTest {
//    private static final String LOCALHOST = "redis://127.0.0.1:6379";
//
//    private RedissonClient redisClient;
//
//
//    private static ExecutorService executorService = Executors.newScheduledThreadPool(10);
//
//    @Before
//    public void init() {
//        Config config = new Config();
//        config.useSingleServer().setAddress(LOCALHOST);
//        redisClient = Redisson.create(config);
//    }
//
//    @Test
//    public void test1() throws InterruptedException {
//        for (int i = 0; i < 10; i++) {
//            executorService.submit(() -> {
//                String threadName = Thread.currentThread().getName();
//                log.info("线程:{}开始抢锁", threadName);
//                val v = redisClient.getLock("lock:test");
//                if (v.isLocked()) {
//                    log.info("线程:{}获取锁成功!", threadName);
//                    try {
//                        TimeUnit.SECONDS.sleep(5);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    log.info("当前线程:{}获取到的结果是:{}", Thread.currentThread().getName(), v);
//                }
//            });
//        }
//        TimeUnit.SECONDS.sleep(100);
//        log.info("程序结束");
//    }
//}
