package com.nuwa.common.oss;

import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

@Data
@Slf4j
public class MinioTest {

    private static MinioClient minioClient;
    @BeforeAll
    public static void init(){
        try {
            minioClient = new MinioClient("http://172.17.0.2:9000/","minioadmin","minioadmin");
        } catch (InvalidEndpointException e) {
            e.printStackTrace();
        } catch (InvalidPortException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateBucket() throws Exception {
        minioClient.makeBucket("test");
        log.info("bucketName:{}创建成功！","test");
    }

    @Test
    public void testUpload()throws Exception{
        File file = new File("/home/Jeffrey/Downloads/apache-skywalking-apm-8.9.1.tar.gz");
        InputStream in = new FileInputStream(file);
        long start = System.currentTimeMillis();
        minioClient.putObject("test", "apache-skywalking-apm-8.9.1.tar.gz", in, new PutObjectOptions(file.length(), -1));
        log.info("耗时:{}s",(System.currentTimeMillis() - start)/1000.0);
    }

}
