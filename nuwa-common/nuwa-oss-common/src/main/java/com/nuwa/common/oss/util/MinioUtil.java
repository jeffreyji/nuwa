package com.nuwa.common.oss.util;

import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.InputStream;

@Component
public class MinioUtil {

    @Autowired
    private MinioClient minioClient;

    /**
     * 创建bucket
     *
     * @param bucketName
     */
    public void createBucket(String bucketName) {
        if (!StringUtils.hasLength(bucketName)) {
            throw new RuntimeException("bucket 不能为空");
        }
        try {
            if (!minioClient.bucketExists(bucketName)) {
                minioClient.makeBucket(bucketName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件
     *
     * @param bucketName
     * @param fileName
     * @param in
     * @param fileSize
     */
    public void upload(String bucketName, String fileName, InputStream in, long fileSize) {
        try {
            minioClient.putObject(bucketName, fileName, in, new PutObjectOptions(fileSize, -1));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取url地址
     *
     * @param bucketName
     * @param fileName
     * @return
     */
    public String getUrl(String bucketName, String fileName) {
        try {
            return minioClient.getObjectUrl(bucketName, fileName);
        } catch (Exception e) {
            throw new RuntimeException("获取url地址失败!");
        }
    }
}
