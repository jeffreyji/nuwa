package com.nuwa.common.oss.config;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class MinioConfig {

    @Value("${oss.url:'http://172.17.0.2:9000/'}")
    private String url;
    @Value("${oss.userName:minioadmin}")
    private String userName;
    @Value("${oss.pwd:minioadmin}")
    private String pwd;

    @Bean
    public MinioClient getMinioClient() {
        try {
            return new MinioClient(url, userName, pwd);
        } catch (Exception e) {
            log.error("minio 初始化异常:{}", e.getMessage(), e);
            throw new RuntimeException("minio-MinioClient init error");
        }
    }
}
