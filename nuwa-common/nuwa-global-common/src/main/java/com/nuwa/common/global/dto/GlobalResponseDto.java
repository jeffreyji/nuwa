package com.nuwa.common.global.dto;

import com.nuwa.common.global.enums.ErrorCodeEnum;
import com.nuwa.common.global.expection.ServiceAssert;
import com.nuwa.common.global.expection.ServiceException;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author jijunhui
 * @date 2021/2/20
 * @desc
 */
@Data
@Accessors(chain = true)
public class GlobalResponseDto<T> implements Serializable {
    /**
     * 返回消息体
     */
    private T data;
    /**
     * 错误码
     */
    private String code;
    /**
     * 错误描述
     */
    private String message;
    /**
     * 请求ID
     */
    private String requestId;
    /**
     * 返回时间
     */
    private String responseTime;

    private GlobalResponseDto(T data) {
        this.data = data;
        this.code = ErrorCodeEnum.SUCCESS.getCode();
        this.message = ErrorCodeEnum.SUCCESS.getMessage();
    }

    private GlobalResponseDto() {
        this.code = ErrorCodeEnum.SUCCESS.getCode();
        this.message = ErrorCodeEnum.SUCCESS.getMessage();
    }

    private GlobalResponseDto(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private GlobalResponseDto(ErrorCodeEnum resultCodeEnum) {
        this.code = resultCodeEnum.getCode();
        this.message = resultCodeEnum.getMessage();
    }

    public static GlobalResponseDto fail(ErrorCodeEnum resultCodeEnum) {
        return new GlobalResponseDto(resultCodeEnum);
    }

    public static GlobalResponseDto fail(ServiceException serviceException) {
        return new GlobalResponseDto(serviceException.getCode(), serviceException.getMessage());
    }

    public static GlobalResponseDto fail(String code, String message) {
        return new GlobalResponseDto(code, message);
    }

    public static GlobalResponseDto fail(String message) {
        return new GlobalResponseDto(ErrorCodeEnum.PARAM_ERROR.getCode(), message);
    }

    public static GlobalResponseDto success(Object data) {
        return new GlobalResponseDto(data);
    }

    public static GlobalResponseDto success() {
        return new GlobalResponseDto();
    }

    public void check() {
        if (!ErrorCodeEnum.SUCCESS.getCode().equals(this.code)) {
            throw new ServiceException(this);
        }
    }

    public void checkDataEmpty(ErrorCodeEnum errorCodeEnum) {
        check();
        ServiceAssert.notNull(this.data,errorCodeEnum);
    }
}
