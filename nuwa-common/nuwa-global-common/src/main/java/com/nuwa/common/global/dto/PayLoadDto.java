package com.nuwa.common.global.dto;

import lombok.Data;

import java.util.Map;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/11/18 17:20
 * @description jwt payLoad
 */
@Data
public class PayLoadDto {
    // 签发人
    private String iss;
    // 受众
    private String aud;
    // 过期时间
    private Long exp;
    // 生效时间
    private Long nbf;
    // 签发时间
    private Long iat;
    // 主体
    private String sub;
    /**
     * 扩展配置
     */
    private Map<String, Object> extend;
}
