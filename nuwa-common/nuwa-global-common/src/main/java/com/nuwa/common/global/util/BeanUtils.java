package com.nuwa.common.global.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author jijunhui
 * @date 2020/6/20
 * @desc
 */
//@Slf4j
public class BeanUtils {

    public static <T> T conversion(Object o, Class<T> c) {
        if (!(o instanceof String)) {
            o = JSON.toJSONString(o);
        }
        return JSONObject.parseObject((String) o, c);
    }

    public static <T> T conversion(Object o, TypeReference<T> tTypeReference) {
        if (!(o instanceof String)) {
            o = JSON.toJSONString(o);
        }
        return JSONObject.parseObject((String) o, tTypeReference);
    }

    /**
     * 多线程list拷贝
     *
     * @param fromList  拷贝前的值
     * @param toClass   拷贝类型
     * @param threshold 开启多线程的阈值 默认是100
     * @return 拷贝后的值
     */
    public static <T> List<T> parallelConversionList(List fromList, Class<T> toClass, Integer threshold) {
        if (null == fromList || fromList.size() == 0) {
            return null;
        }
        if (null == threshold || threshold < 100) {
            threshold = 100;
        }
        try {
            if (fromList.size() < threshold) {
                List toList = new ArrayList();
                for (Object aFromList : fromList) {
                    toList.add(conversion(aFromList, toClass));
                }
                return toList;
            } else {
                List syncList = Collections.synchronizedList(new ArrayList<>());
                fromList.parallelStream().forEach(from -> {
                    syncList.add(conversion(from, toClass));
                });
                return syncList;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 多线程list拷贝
     *
     * @param fromList 拷贝前的值
     * @param toClass  拷贝类型
     * @return 拷贝后的值
     */
    public static <T> List<T> parallelConversionList(List fromList, Class<T> toClass) {
        return parallelConversionList(fromList, toClass, null);
    }

    /**
     * 单线程list转换
     *
     * @param fromList
     * @param toClass
     * @param <T>
     * @return
     */
    public static <T> List<T> conversionList(List fromList, Class<T> toClass) {
        if (null == fromList || fromList.size() == 0) {
            return null;
        }
        try {
            List toList = new ArrayList();
            for (Object aFromList : fromList) {
                toList.add(conversion(aFromList, toClass));
            }
            return toList;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取对象中值为空的属性
     *
     * @param source
     * @return
     */
    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }


//    public static void main(String[] args) {
//        C c = new C();
//        c.setSex(0);
//
//        A a = new A();
//        a.setAge(18);
//        a.setEmail("jjh@qq.com");
//        a.setName("嘿嘿");
//        a.setBirthDay(new Date());
//        a.setPhone("18500237786");
//        a.setC(c);
//        System.out.println("a:" + a.toString());
//
//        B b = conversion(a, B.class);
//        System.out.println("b:" + b.toString());
//
//        List<A> list = new ArrayList<>();
//        for (int j = 0; j < 10; j++) {
//            for (int i = 0; i < 20000; i++) {
//                C c1 = new C();
//                c1.setSex(i);
//                A a1 = new A();
//                a1.setAge(10 + i);
//                a1.setEmail("jjh@qq.com" + i);
//                a1.setName("嘿嘿" + i);
//                a1.setBirthDay(new Date());
//                a1.setPhone("18500237786" + i);
//                a1.setC(c1);
//                list.add(a1);
//            }
//            System.out.println("a list size:" + list.size());
//            long start = System.currentTimeMillis();
//            conversionList(list, B.class);
//            System.out.println("单线程耗时:" + (System.currentTimeMillis() - start) + "ms" + "，第   " + j + "次");
//
//            start = System.currentTimeMillis();
//            parallelConversionList(list, B.class, 100);
//            System.out.println("多线程耗时:" + (System.currentTimeMillis() - start) + "ms" + "，第   " + j + "次");
//        }
//    }

}

//@Data
//class A {
//    private String name;
//    private int age;
//    private String email;
//    private Date birthDay;
//    private String phone;
//    private C c;
//}
//
//@Data
//class B {
//    private String name;
//    private int age;
//    private String email;
//    private Date birthDay;
//    private C c;
//}
//
//@Data
//class C {
//    private int sex;
//}
