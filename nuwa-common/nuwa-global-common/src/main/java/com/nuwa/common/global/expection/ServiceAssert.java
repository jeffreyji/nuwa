package com.nuwa.common.global.expection;

import com.nuwa.common.global.enums.ErrorCodeEnum;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;

/**
 * 异常统一处理类
 */
public class ServiceAssert {
    /**
     * 如果obj是空 则抛出异常
     *
     * @param obj
     * @param msg
     */
    public final static void notNull(Object obj, @NotNull Object msg) {
        if (null == obj) {
            throwException(msg);
        }
    }

    /**
     * 字符串为空 则抛出异常
     *
     * @param str
     * @param msg
     */
    public final static void stringHasLength(String str, Object msg) {
        if (str != null && !str.trim().isEmpty()) {
            throwException(msg);
        }
    }

    /**
     * 集合为空 则抛出异常
     *
     * @param collection
     * @param msg
     */
    public final static void collectionHasLength(Collection collection, Object msg) {
        if (collection != null && !collection.isEmpty()) {
            throwException(msg);
        }
    }

    /**
     * Map为空 则抛出异常
     *
     * @param map
     * @param msg
     */
    public final static void mapHasLength(Map map, Object msg) {
        if (map != null && !map.isEmpty()) {
            throwException(msg);
        }
    }

    /**
     * 如果没有长度则抛出异常 支持字符串、集合、map
     *
     * @param obj
     * @param msg
     */
    public final static void hasLength(Object obj, Object msg) {
        notNull(obj, msg);
        collectionHasLength((Collection) obj, msg);
        mapHasLength((Map) obj, msg);
        stringHasLength(obj.toString(), msg);
    }


    public final static void throwException(Object msg) {
        if (msg instanceof ErrorCodeEnum) {
            throw new ServiceException((ErrorCodeEnum) msg);
        } else {
            throw new ServiceException(msg.toString());
        }
    }

    public static void main(String[] args) {
//        ServiceAssert.notNull(null, ErrorCodeEnum.PARAM_ERROR);
        ServiceAssert.notNull(null, "用户不能为空");
    }
}
