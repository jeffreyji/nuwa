package com.nuwa.common.global.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * 请求头
 */
@Data
public class RequestHeaderDto implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 请求id
     */
    private String requestId;
    /**
     * appId
     */
    @NotEmpty(message = "公司ID不可以为空")
    private String appId;
    /**
     * 请求时间
     */
    private String requestTime;
    /**
     * 请求客户端 1:苹果 2:安卓 3:pc  10:微信 11:微信公众号 12:微信小程序  20:支付宝 21:支付宝小程序
     */
    private String requestClient;
    /**
     * 版本
     */
    private String version;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户token
     */
    private String token;
    /**
     * 刷新token
     */
    private String refreshToken;
    /**
     * 设备ID 1. 移动端就是设备唯一标识，可能为空
     */
    private String deviceId;
    /**
     * 手机号
     */
    private String mobileNo;

    /**
     * 手机型号
     */
    private String mobileModel;
    /**
     * 手机品牌
     */
    private String brand;
    /**
     * 设备系统版本
     */
    private String systemVersion;
}
