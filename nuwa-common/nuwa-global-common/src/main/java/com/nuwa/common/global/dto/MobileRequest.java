package com.nuwa.common.global.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2021/3/5 16:29
 * @description 移动端请求对象
 */
@Data
public class MobileRequest extends BaseRequest {

    /**
     * 设备ID 1. 移动端就是设备唯一标识，可能为空
     */
    @NotEmpty(message = "设备ID不能为空")
    private String deviceId;
    /**
     * 手机号
     */
    private String mobileNo;

    /**
     * 手机型号
     */
    private String mobileModel;
    /**
     * 手机品牌
     */
    private String brand;
    /**
     * 设备系统版本
     */
    private String systemVersion;

}
