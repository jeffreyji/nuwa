package com.nuwa.common.global.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2021/3/5 16:17
 * @description 基础请求类
 */
@Data
public class BaseRequest implements Serializable {
    /**
     * 请求id
     */
    private String requestId;
    /**
     * 请求时间
     */
    private String requestTime;
    /**
     * 请求客户端 1:苹果 2:安卓 3:pc  10:微信 11:微信公众号 12:微信小程序  20:支付宝 21:支付宝小程序
     */
    private String requestClientType;
    /**
     * 请求ip地址
     */
    private String requestIp;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 版本信息
     */
    private String version;

}
