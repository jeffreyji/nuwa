package com.nuwa.common.global.advice;

import com.alibaba.fastjson.JSON;
import com.nuwa.common.global.constants.Constants;
import com.nuwa.common.global.dto.GlobalResponseDto;
import com.nuwa.common.global.util.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author jeffrey
 */
@RestControllerAdvice
@Slf4j
public class ControllerResponseAdvice implements ResponseBodyAdvice<Object> {

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public GlobalResponseDto beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        HttpServletRequest httpServletRequest = SpringContextUtils.getHttpServletRequest();
        GlobalResponseDto responseDto;
        if (!(body instanceof GlobalResponseDto)) {
            responseDto = GlobalResponseDto.success(null == body ? new HashMap<>() : body);
        } else {
            responseDto = (GlobalResponseDto) body;
        }
        responseDto.setResponseTime(System.currentTimeMillis() + "");
        String s = responseDto.toString();
        log.info("请求URL:{},返回结果:{}", httpServletRequest.getRequestURL(), s.length() > Constants.LOG_LENGTH ? s.length() : s);
        return responseDto;
    }
}
