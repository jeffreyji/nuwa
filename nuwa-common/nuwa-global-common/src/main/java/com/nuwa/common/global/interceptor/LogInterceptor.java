package com.nuwa.common.global.interceptor;

import com.nuwa.common.global.constants.Constants;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jijunhui
 * @date 2020/11/21
 * @desc
 */
@Slf4j
public class LogInterceptor implements HandlerInterceptor {
    private static final ThreadLocal<Long> threadLocal = new ThreadLocal();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        threadLocal.set(System.currentTimeMillis());
        String logSerialNo = request.getHeader(Constants.LOG_SERIAL_NO);
        if (StringUtils.isEmpty(logSerialNo)) {
            logSerialNo = "123123123123123123123123";
        }

        MDC.put(Constants.LOG_SERIAL_NO, logSerialNo);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        MDC.clear();
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        String url = request.getRequestURL().toString();
        Long startTime = threadLocal.get();
        if (null != startTime) {
            log.info("请求URL:{},请求开始时间：{},耗时：{} ms", url, startTime, (System.currentTimeMillis() - startTime));
        }
        threadLocal.remove();
    }
}
