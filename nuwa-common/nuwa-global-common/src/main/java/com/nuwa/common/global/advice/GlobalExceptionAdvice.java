package com.nuwa.common.global.advice;

import com.nuwa.common.global.dto.GlobalResponseDto;
import com.nuwa.common.global.enums.ErrorCodeEnum;
import com.nuwa.common.global.expection.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * @author jeffrey
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionAdvice {
    /**
     * 业务异常
     *
     * @param commonException
     * @return
     */
    @ExceptionHandler({ServiceException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GlobalResponseDto processMessage(ServiceException commonException) {
        log.error("业务异常：错误码={}，错误描述={}", commonException.getCode(), commonException.getMessage(), commonException);
        return GlobalResponseDto.fail(commonException.getCode(), commonException.getMessage());
    }

    /**
     * 方法不支持
     *
     * @param exception
     * @return
     */
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GlobalResponseDto paramError(HttpRequestMethodNotSupportedException exception) {
        log.error("方法不支持：{}", exception.getMessage(), exception);
        return GlobalResponseDto.fail(ErrorCodeEnum.METHOD_NOT_SUPPORT);
    }

    /**
     * 系统异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public GlobalResponseDto processMessage(Exception exception) {
        log.error("系统异常：错误信息={}", exception.getMessage(), exception);
        return GlobalResponseDto.fail(ErrorCodeEnum.SYSTEM_ERROR);
    }

    /**
     * 地址不存在异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public GlobalResponseDto notFountHandler(NoHandlerFoundException exception) {
        log.error("地址不存在：{}", exception.getMessage(), exception);
        return GlobalResponseDto.fail(ErrorCodeEnum.URL_NOT_FOUND);
    }
}
