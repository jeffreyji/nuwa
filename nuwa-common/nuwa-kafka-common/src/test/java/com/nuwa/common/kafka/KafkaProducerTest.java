package com.nuwa.common.kafka;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * kafka生产者单元测试
 */
@Slf4j
public class KafkaProducerTest {

    private static Properties props = new Properties();

    private static final String TOPIC = "jeffrey";

    private static KafkaProducer<String, String> kafkaProducer;

    private static final String BOOTSTRAT_SERVERS = "localhost:9092";

    @BeforeAll
    public static void init() {
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAT_SERVERS);
        props.put(ProducerConfig.ACKS_CONFIG, "-1");
        // props.put("group.id", "nuwa");
//        props.put("acks", "all");
//        props.put("retries", 0);
//        props.put("batch.size", 16384);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, "TEST");
        props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG,true);
        kafkaProducer = new KafkaProducer(props);
    }

    @AfterAll
    public static void close() {
        kafkaProducer.close();
    }

    @Test
    public void testSendMsg() throws ExecutionException, InterruptedException {
        for (; ; ) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(TOPIC, ThreadLocalRandom.current().nextInt() + "");
            Future<RecordMetadata> result = kafkaProducer.send(producerRecord, (recordMetadata, e) -> {
                log.info("发送消息成功:topic:{},partition:{},offset", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset());
            });
        }
    }

    @Test
    public void testSendTransactionMsg() throws Exception {
        kafkaProducer.initTransactions();
        kafkaProducer.beginTransaction();
        try {
            for (int i = 0; i < 10; i++) {
                ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>(TOPIC, "事务消息:" + i);
                kafkaProducer.send(producerRecord);
                log.info("发送事物消息:{}成功",producerRecord.value());
                TimeUnit.SECONDS.sleep(i + 1);
            }
            // 提交事务
            kafkaProducer.commitTransaction();
            log.info("事物提交成功！");
        } catch (Exception e) {
            e.printStackTrace();
            // 事务拒绝
            kafkaProducer.abortTransaction();
            log.info("事物提交失败,回滚");
        }
    }
}
