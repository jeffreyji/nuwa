package com.nuwa.common.kafka;

import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Log4j2
public class KafkaInterceptTest {
    private static Properties props = new Properties();

    private static KafkaProducer<String, String> kafkaProducer;

    @BeforeAll
    public static void init() {
        props.put("bootstrap.servers", "127.0.0.1:9092");
//        props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG,KafkaCustomIntercept.class.getName());
        // props.put("group.id", "nuwa");
//        props.put("acks", "all");
//        props.put("retries", 0);
//        props.put("batch.size", 16384);
//        props.put(ProducerConfig.ACKS_CONFIG,"-1");
        props.put("key.serializer", StringSerializer.class.getName());
        props.put("value.serializer", StringSerializer.class.getName());
        kafkaProducer = new KafkaProducer(props);
    }

    @AfterAll
    public static void close() {
        kafkaProducer.close();
    }

    @Test
    public void testSendMsg() throws ExecutionException, InterruptedException {
        for (int i = 0; i < 100; i++) {
            ProducerRecord<String, String> record = new ProducerRecord<>("test-topic1", "send-key", "send-value");
            final val send = kafkaProducer.send(record);
            final val recordMetadata = send.get();
            log.info("send msg:{},{},{},{}", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), recordMetadata.toString());
        }
    }
}

