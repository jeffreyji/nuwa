package com.nuwa.common.kafka;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class KafkaConsumerTest {
    private static Properties props = new Properties();

    private static KafkaConsumer<String, String> kafkaConsumer;

    private static String topics = "jeffrey";
    private static final String BOOTSTRAT_SERVERS = "localhost:9092";

    @BeforeAll
    public static void init() {
        props.put(ConsumerConfig.GROUP_ID_CONFIG,"test_time");
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,BOOTSTRAT_SERVERS);
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class.getName());
        props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG,"read_committed");
        kafkaConsumer = new KafkaConsumer(props);
    }

    @AfterAll
    public static void close() {
        kafkaConsumer.close();
    }

    @Test
    public void testHandleMsg() throws ExecutionException, InterruptedException {
        kafkaConsumer.subscribe(Arrays.asList(topics.split(",")));
        for (; ; ) {
            final val pollMsg = kafkaConsumer.poll(Duration.of(100, ChronoUnit.MILLIS));
            if (!pollMsg.isEmpty()) {
                log.info("本次拉去到的消息数量:{}", pollMsg.count());
                pollMsg.forEach(msg -> log.info("topic:{},value:{},partition:{},offset:{}", msg.topic(),msg.value(),msg.partition(),msg.offset()));
            } else {
                log.info("本次没有拉取到任何消息，睡眠1s");
                TimeUnit.SECONDS.sleep(1);
            }
        }
    }

    @Test
    public void testHandleMsgByTime() throws InterruptedException {
        long time = 1849064329574L;
//        kafkaConsumer.subscribe(Arrays.asList(topics.split(",")));
        final Map<TopicPartition,Long> timeMap = new HashMap<>();
        val partitionInfos = kafkaConsumer.partitionsFor(topics);
        List<TopicPartition> topicPartitions = new ArrayList<>();
        partitionInfos.forEach(partition ->{
            TopicPartition topicPartition = new TopicPartition(partition.topic(),partition.partition());
            timeMap.put(topicPartition,time);
            topicPartitions.add(topicPartition);
        });

        kafkaConsumer.assign(topicPartitions);
        Map<TopicPartition, OffsetAndTimestamp> newOffset = kafkaConsumer.offsetsForTimes(timeMap);
        for (Map.Entry<TopicPartition, OffsetAndTimestamp> entry : newOffset.entrySet()) {
            log.info("entry:{}",entry);
            // 时间超过最大offset的情况
            if (null != entry){
                kafkaConsumer.seek(entry.getKey(), entry.getValue().offset());//每个topic的partition都seek到执行的offset
            }
        }
        for (;;){
            final val pollMsg = kafkaConsumer.poll(Duration.of(100, ChronoUnit.MILLIS));
            if (!pollMsg.isEmpty()) {
                log.info("本次拉去到的消息数量:{}", pollMsg.count());
                pollMsg.forEach(msg -> log.info("topic:{},value:{},partition:{},offset:{}", msg.topic(),msg.value(),msg.partition(),msg.offset()));
            } else {
                log.info("本次没有拉取到任何消息，睡眠5s");
                TimeUnit.SECONDS.sleep(5);
            }
        }
    }

}
