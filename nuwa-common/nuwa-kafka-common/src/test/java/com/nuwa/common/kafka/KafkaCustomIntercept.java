package com.nuwa.common.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.nio.charset.StandardCharsets;
import java.util.Map;


/**
 * 自定义拦截器
 */
@Slf4j
public class KafkaCustomIntercept implements ProducerInterceptor {
    @Override
    public ProducerRecord onSend(ProducerRecord record) {
        log.info("需要发送的消息内容:{}", record);
//        ProducerRecord<String,String> producerRecord = new ProducerRecord<>(record.topic(), record.key(), record.value());
        record.headers().add("kkkkkkkkkk", "cccccccccccccccc".getBytes(StandardCharsets.UTF_8));
        log.info("拦截后的内容:{}", record);
        return record;
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {
        log.info("metadata:{}", metadata);
        log.error("exception:{}", exception.getMessage());
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {
        log.info("configure:{}", configs);
    }
}
