package com.nuwa.common.kafka;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * kafka配置管理
 */
@Slf4j
public class KafkaTopicManagerTest {

    private static Properties props = new Properties();
    private static AdminClient adminClient;

    @BeforeAll
    public static void init() {
        props.put("bootstrap.servers", "127.0.0.1:9092");
        // props.put("group.id", "nuwa");
//        props.put("acks", "all");
//        props.put("retries", 0);
//        props.put("batch.size", 16384);
//        props.put("key.serializer", StringSerializer.class.getName());
//        props.put("value.serializer", StringSerializer.class.getName());
        adminClient = AdminClient.create(props);
    }

    @AfterAll
    public static void close() {
        adminClient.close();
    }


    @Test
    public void testListTopics() throws ExecutionException, InterruptedException {
        final val listTopicsResult = adminClient.listTopics();
        listTopicsResult.listings().get().forEach(System.out::println);
        adminClient.close();
    }

    @Test
    public void testCreateTopic() throws ExecutionException, InterruptedException {
        //创建topic选项
//        CreateTopicsOptions createTopicsOptions = new CreateTopicsOptions();
//
//        createTopicsOptions.timeoutMs(20000);
        NewTopic topic = new NewTopic("test-topic3", 3, (short) 1);
        final CreateTopicsResult topics = adminClient.createTopics(Arrays.asList(topic));
        topics.all().get();
        log.info("topics:{}", topics);

    }

    @Test
    public void testGetTopicInfo() throws ExecutionException, InterruptedException {
        final val describeTopicsResult = adminClient.describeTopics(Arrays.asList("test-topic2"));
        final val allInfos = describeTopicsResult.all();
        final val stringTopicDescriptionMap = allInfos.get();
        final val topicDescription = stringTopicDescriptionMap.get("test-topic2");
        log.info("topic description:{},{}", topicDescription.name(), topicDescription.toString());
    }

    @Test
    public void testDelTopic() throws ExecutionException, InterruptedException {
        final val deleteTopicsResult = adminClient.deleteTopics(Arrays.asList("test-topic2"));
        log.info("delete topic:{}", deleteTopicsResult);
        testListTopics();
    }

}
