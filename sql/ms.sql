/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : ms

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 29/10/2020 20:01:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_good
-- ----------------------------
DROP TABLE IF EXISTS `tb_good`;
CREATE TABLE `tb_good`
(
    `ID`          bigint                                                 NOT NULL AUTO_INCREMENT COMMENT '商品ID',
    `NAME`        varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
    `PRICE`       decimal(10, 2)                                         NULL DEFAULT NULL,
    `COUNT`       bigint                                                 NULL DEFAULT 0 COMMENT '商品库存，-1表示没有限制',
    `START_TIME`  datetime(0)                                            NULL DEFAULT NULL COMMENT '开始时间',
    `END_TIME`    datetime(0)                                            NULL DEFAULT NULL COMMENT '结束时间',
    `CREATE_TIME` datetime(0)                                            NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_good
-- ----------------------------
INSERT INTO `tb_good`
VALUES (1, 'iphoneX', 7788.00, 0, NULL, NULL, '2020-10-29 14:36:10');
INSERT INTO `tb_good`
VALUES (2, '华为 Mate 10', 4199.00, 50, NULL, NULL, '2020-10-29 14:36:14');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`
(
    `ID`          bigint         NOT NULL AUTO_INCREMENT,
    `USER_ID`     bigint         NULL DEFAULT NULL,
    `GOOD_ID`     bigint         NULL DEFAULT NULL,
    `PRICE`       decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
    `GOOD_COUNT`  int            NULL DEFAULT NULL,
    `STATUS`      tinyint        NULL DEFAULT NULL COMMENT '订单状态，0新建未支付，1已支付，2已发货，3已收货，4已退款，5已完成',
    `CREATE_TIME` datetime(0)    NULL DEFAULT NULL,
    `PAY_DATE`    datetime(0)    NULL DEFAULT NULL,
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 11
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order`
VALUES (1321783948204720130, 3, 1, 7788.00, 1, 0, NULL, NULL);
INSERT INTO `tb_order`
VALUES (1321783949102301185, 8, 1, 7788.00, 1, 0, NULL, NULL);
INSERT INTO `tb_order`
VALUES (1321783949983105025, 17, 1, 7788.00, 1, 0, NULL, NULL);
INSERT INTO `tb_order`
VALUES (1321783950385758209, 24, 1, 7788.00, 1, 0, NULL, NULL);
INSERT INTO `tb_order`
VALUES (1321783950733885442, 32, 1, 7788.00, 1, 0, NULL, NULL);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`
(
    `ID`              bigint UNSIGNED                                         NOT NULL COMMENT '用户id',
    `NICK_NAME`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
    `PASSWORD`        varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT 'MD5(MD5(pass明文+固定salt)+salt',
    `SALT`            varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '混淆盐',
    `HEAD`            varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像，云存储的ID',
    `REGISTER_DATE`   datetime(0)                                             NULL DEFAULT NULL COMMENT '注册时间',
    `LAST_LOGIN_DATE` datetime(0)                                             NULL DEFAULT NULL COMMENT '上次登录时间',
    `LOGIN_COUNT`     int                                                     NULL DEFAULT NULL COMMENT '登录次数',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user`
VALUES (18181818181, 'jesper', 'b7797cce01b4b131b433b6acf4add449', '1a2b3c4d', NULL, '2018-05-21 21:10:21',
        '2018-05-21 21:10:25', 1);
INSERT INTO `tb_user`
VALUES (18217272828, 'jesper', 'b7797cce01b4b131b433b6acf4add449', '1a2b3c4d', NULL, '2018-05-21 21:10:21',
        '2018-05-21 21:10:25', 1);

SET FOREIGN_KEY_CHECKS = 1;
