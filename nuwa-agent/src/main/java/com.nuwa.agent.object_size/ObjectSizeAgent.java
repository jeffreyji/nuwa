package com.nuwa.agent.object_size;

import java.lang.instrument.Instrumentation;

/**
 * @Author jijunhui
 * @Date 2021/5/29 7:21
 * @Version 1.0.0
 * @Description
 */
public class ObjectSizeAgent {
    private static Instrumentation instrumentation;


    public static void premain(String agentArgs, Instrumentation _inst) {
        instrumentation = _inst;
        System.out.println("instrumentation:" + instrumentation);
    }

    public static long sizeOf(Object obj) {
        return instrumentation.getObjectSize(obj);
    }
}
