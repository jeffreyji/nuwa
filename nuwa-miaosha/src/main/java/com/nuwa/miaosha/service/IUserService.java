package com.nuwa.miaosha.service;

import com.nuwa.miaosha.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface IUserService extends IService<User> {
    /**
     * 用户注册
     * @param user
     */
    String register(User user);

    /**
     * 用户注册
     * @param account
     * @param pwd
     */
    String login(Long phone,String pwd);

}
