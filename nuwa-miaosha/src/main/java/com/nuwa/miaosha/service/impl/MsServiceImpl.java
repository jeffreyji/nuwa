package com.nuwa.miaosha.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nuwa.common.global.expection.ServiceException;
import com.nuwa.miaosha.entity.Good;
import com.nuwa.miaosha.entity.Ms;
import com.nuwa.miaosha.entity.Subject;
import com.nuwa.miaosha.mapper.MsMapper;
import com.nuwa.miaosha.service.IGoodService;
import com.nuwa.miaosha.service.IMsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.miaosha.service.IOrderService;
import com.nuwa.miaosha.service.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 秒杀表 服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Service
public class MsServiceImpl extends ServiceImpl<MsMapper, Ms> implements IMsService {

    @Autowired
    private ISubjectService subjectService;
    @Autowired
    private IGoodService goodService;
    @Autowired
    private IOrderService orderService;
    @Autowired
    private IMsService msService;

    /**
     * 秒杀接口
     * 1. 校验活动是否在有效期内
     * 2. 校验商品是否在有效期内
     * 3. 校验商品是否还有库存
     * 4. 校验用户token是否在有效期内
     * 5. 校验是否已经秒杀过该产品
     * 6. 生成订单
     * 7. 扣减库存
     *
     * @param subjectId
     * @param goodId
     */
    @Override
    public void ms(Long subjectId, Long goodId) {
        Subject subject = subjectService.getById(subjectId);
        // 校验活动信息
        validateSubject(subject);
        // 秒杀信息
        Ms msOne = msService.getOne(new LambdaQueryWrapper<>(Ms.class).eq(Ms::getGoodId, goodId).eq(Ms::getSubjectId, subject));
        // 校验秒杀信息
        validateMs(msOne);
        // 查询订单信息
        String userId = "";

    }

    // 校验活动
    private void validateSubject(Subject subject) {
        if (null == subject) {
            throw new ServiceException("活动不存在");
        }
        if (subject.getBeginTime().isAfter(LocalDateTime.now())) {
            throw new ServiceException("活动还未开始");
        }
        if (subject.getEndTime().isBefore(LocalDateTime.now())) {
            throw new ServiceException("活动已经结束");
        }
        if (subject.getStatus() > 0) {
            throw new ServiceException("活动已经失效");
        }
    }

    // 校验活动
    private void validateMs(Ms msOne) {
        if (null == msOne) {
            throw new ServiceException("商品不存在");
        }
        if (msOne.getBeginTime().isAfter(LocalDateTime.now())) {
            throw new ServiceException("活动还未开始");
        }
        if (msOne.getEndTime().isBefore(LocalDateTime.now())) {
            throw new ServiceException("活动已经结束");
        }
        if (msOne.getStatus() > 0) {
            throw new ServiceException("活动已经失效");
        }
        if (msOne.getStatus() <=0){
            throw new ServiceException("库存不足");
        }
    }
}
