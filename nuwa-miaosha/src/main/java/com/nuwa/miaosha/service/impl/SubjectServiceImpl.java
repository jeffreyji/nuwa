package com.nuwa.miaosha.service.impl;

import com.nuwa.miaosha.entity.Subject;
import com.nuwa.miaosha.mapper.SubjectMapper;
import com.nuwa.miaosha.service.ISubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 主题活动表 服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {

}
