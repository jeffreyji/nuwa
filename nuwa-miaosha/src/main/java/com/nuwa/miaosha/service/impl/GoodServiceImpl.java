package com.nuwa.miaosha.service.impl;

import com.nuwa.miaosha.entity.Good;
import com.nuwa.miaosha.mapper.GoodMapper;
import com.nuwa.miaosha.service.IGoodService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Service
public class GoodServiceImpl extends ServiceImpl<GoodMapper, Good> implements IGoodService {

}
