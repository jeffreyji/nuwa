package com.nuwa.miaosha.service;

import com.nuwa.miaosha.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface IOrderService extends IService<Order> {

}
