package com.nuwa.miaosha.service.impl;

import com.nuwa.miaosha.entity.Order;
import com.nuwa.miaosha.mapper.OrderMapper;
import com.nuwa.miaosha.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

}
