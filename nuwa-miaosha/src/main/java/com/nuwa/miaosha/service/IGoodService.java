package com.nuwa.miaosha.service;

import com.nuwa.miaosha.entity.Good;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface IGoodService extends IService<Good> {

}
