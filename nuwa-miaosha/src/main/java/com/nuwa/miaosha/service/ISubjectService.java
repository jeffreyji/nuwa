package com.nuwa.miaosha.service;

import com.nuwa.miaosha.entity.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 主题活动表 服务类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface ISubjectService extends IService<Subject> {

}
