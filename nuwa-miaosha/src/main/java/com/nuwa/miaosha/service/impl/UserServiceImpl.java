package com.nuwa.miaosha.service.impl;

import com.google.common.hash.Hashing;
import com.nuwa.common.global.expection.ServiceException;
import com.nuwa.common.global.util.JwtTokenUtils;
import com.nuwa.miaosha.cache.UserCache;
import com.nuwa.miaosha.entity.User;
import com.nuwa.miaosha.mapper.UserMapper;
import com.nuwa.miaosha.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.time.LocalDateTime;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserCache userCache;

    /**
     * 用户注册
     *
     * @param user
     */
    @Override
    public String register(User user) {
        User currentUser = baseMapper.selectById(user.getId());
        if (null != currentUser) {
            log.error("用户:{}已经注册", user);
            throw new ServiceException("用户已经注册");
        }
        user.setRegisterTime(LocalDateTime.now());
        user.setLastLoginTime(user.getRegisterTime());
        user.setPassword(pwdMd5(user.getPassword()));
        String token = JwtTokenUtils.createToken(user.getId().toString());
        baseMapper.insert(user);
        // 这里要加入redis缓存
        userCache.saveToken(user.getId().toString(), token, JwtTokenUtils.EXPIRITION);
        return token;
    }

    /**
     * 用户注册
     *
     * @param phone
     * @param pwd
     */
    @Override
    public String login(Long phone, String pwd) {
        User currentUser = baseMapper.selectById(phone);
        if (null == currentUser) {
            log.error("用户:{}还未注册", phone);
            throw new ServiceException("用户还未注册");
        }
        if (!currentUser.getPassword().equals(pwdMd5(pwd))) {
            log.warn("用户名/密码不正确");
            throw new ServiceException("用户名/密码不正确");
        }

        String token = JwtTokenUtils.createToken(currentUser.getId().toString());
        // 这里要加入redis缓存
        userCache.saveToken(currentUser.getId().toString(), token, JwtTokenUtils.EXPIRITION);
        currentUser.setLastLoginTime(LocalDateTime.now());
        baseMapper.updateById(currentUser);
        return token;
    }

    /**
     * 获取md5
     *
     * @param password
     * @return
     */
    private String pwdMd5(String password) {
        return Hashing.md5().newHasher().putString(password, Charset.defaultCharset()).hash().toString();
    }
}
