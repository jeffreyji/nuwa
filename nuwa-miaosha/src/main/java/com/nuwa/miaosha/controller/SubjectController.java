package com.nuwa.miaosha.controller;


import com.nuwa.miaosha.entity.Subject;
import com.nuwa.miaosha.service.ISubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 主题活动表 前端控制器
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@RestController
@RequestMapping("/subject")
public class SubjectController {
    @Autowired
    private ISubjectService subjectService;

    @PostMapping("listAll")
    public List<Subject> listAll() {
        return subjectService.list();
    }

}
