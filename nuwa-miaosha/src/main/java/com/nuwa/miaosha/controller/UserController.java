package com.nuwa.miaosha.controller;


import com.nuwa.miaosha.entity.User;
import com.nuwa.miaosha.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @PostMapping("register")
    private String register(@RequestBody User user) {
        return userService.register(user);
    }

    @PostMapping("login")
    private String login(@RequestParam("phone") Long phone, @RequestParam("pwd") String pwd) {
        return userService.login(phone, pwd);
    }

}
