package com.nuwa.miaosha.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_good")
public class Good implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "ID",type = IdType.ID_WORKER)
    private Long id;

    /**
     * 商品编码
     */
    @TableField("CODE")
    private String code;

    /**
     * 商品名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 商品标题
     */
    @TableField("TITLE")
    private String title;

    /**
     * 价格
     */
    @TableField("PRICE")
    private BigDecimal price;

    /**
     * 商品详情
     */
    @TableField("DETAIL")
    private String detail;

    @TableField("STOCK")
    private Long stock;


}
