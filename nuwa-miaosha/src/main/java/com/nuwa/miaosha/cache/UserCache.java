package com.nuwa.miaosha.cache;

import com.nuwa.common.cache.util.RedissonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserCache {
    @Autowired
    private RedissonUtil redissonUtil;

    /**
     * 保存token
     *
     * @param userId
     * @param token
     * @param expired
     */
    public void saveToken(String userId, String token, long expired) {
        redissonUtil.setStr(userId, token, expired);
    }
}
