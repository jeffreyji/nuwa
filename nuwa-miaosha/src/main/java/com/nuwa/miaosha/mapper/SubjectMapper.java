package com.nuwa.miaosha.mapper;

import com.nuwa.miaosha.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 主题活动表 Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface SubjectMapper extends BaseMapper<Subject> {

}
