package com.nuwa.miaosha.mapper;

import com.nuwa.miaosha.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface OrderMapper extends BaseMapper<Order> {

}
