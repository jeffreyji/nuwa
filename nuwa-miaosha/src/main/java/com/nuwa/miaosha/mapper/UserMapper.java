package com.nuwa.miaosha.mapper;

import com.nuwa.miaosha.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface UserMapper extends BaseMapper<User> {

}
