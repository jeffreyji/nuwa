package com.nuwa.miaosha.mapper;

import com.nuwa.miaosha.entity.Ms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秒杀表 Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface MsMapper extends BaseMapper<Ms> {

}
