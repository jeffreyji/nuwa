package com.nuwa.miaosha.mapper;

import com.nuwa.miaosha.entity.Good;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jeffrey
 * @since 2022-09-05
 */
public interface GoodMapper extends BaseMapper<Good> {

}
