package com.nuwa.miaosha.service;

import com.nuwa.miaosha.entity.Subject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Date;

@SpringBootTest
@Slf4j
public class IMsServiceTest {
    @Autowired
    private ISubjectService subjectService;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    @Test
    public void init(){
        Subject subject = new Subject();
        subject.setBeginTime(LocalDateTime.now());
        subject.setEndTime(LocalDateTime.now());
        subject.setName("中秋豪礼");
        subject.setStatus(0);
        subject.setCreateTime(LocalDateTime.now());
        subjectService.save(subject);
    }
}