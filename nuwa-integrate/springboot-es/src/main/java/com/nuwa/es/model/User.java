//package com.nuwa.es.model;
//
//import lombok.Data;
//import org.springframework.data.annotation.Id;
//import org.springframework.data.elasticsearch.annotations.Document;
//import org.springframework.data.elasticsearch.annotations.Field;
//import org.springframework.data.elasticsearch.annotations.FieldType;
//
//import java.util.Date;
//
///**
// * @Author jijunhui
// * @Date 2021/6/16 22:12
// * @Version 1.0.0
// * @Description
// */
//@Data
//@Document(indexName = "d_user",shards = 5)
//public class User {
//    @Id
//    private Long id;
//    /**
//     * 姓
//     */
//    @Field(type = FieldType.Keyword)
//    private String firstName;
//    /**
//     * 名字
//     */
//    @Field(type = FieldType.Text)
//    private String name;
//    /**
//     * 年龄
//     */
//    @Field(type = FieldType.Integer)
//    private int age;
//    /**
//     * 生日
//     */
//    @Field(type = FieldType.Date)
//    private Date birthday;
//    /**
//     * 个人描述
//     */
//    @Field(type = FieldType.Text)
//    private String desc;
//
//}
