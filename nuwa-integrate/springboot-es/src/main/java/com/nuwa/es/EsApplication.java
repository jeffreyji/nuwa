package com.nuwa.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author jijunhui
 * @Date 2021/6/16 22:09
 * @Version 1.0.0
 * @Description
 */
@SpringBootApplication(scanBasePackages = {"com.nuwa"})
public class EsApplication {
    public static void main(String[] args) {
        SpringApplication.run(EsApplication.class, args);
    }
}
