package com.nuwa.es.controller;

import com.nuwa.common.es.util.ElasticUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author jijunhui
 * @Date 2021/6/16 22:37
 * @Version 1.0.0
 * @Description
 */
@RestController
@Slf4j
public class EsUserController {
    private final static String INDEX_NAME = "user";
    @Autowired
    private ElasticUtils elasticUtils;

    @RequestMapping("index/create")
    public Boolean createIndex() {
        Map<String, Object> columnMap = new HashMap<>();
        // ID
        Map<String, Object> id = new HashMap<>();
        id.put("type", "integer");
        columnMap.put("id", id);
        // 姓名
        Map<String, Object> name = new HashMap<>();
        name.put("type", "keyword");
        columnMap.put("name", name);
        // 年龄
        Map<String, Object> age = new HashMap<>();
        age.put("type", "integer");
        columnMap.put("age", age);
        // 创建
        Map<String, Object> createTime = new HashMap<>();
        createTime.put("type", "long");
        columnMap.put("createTime", createTime);
        return elasticUtils.createIndex(INDEX_NAME, columnMap);
    }

    /**
     * 保存用户
     *
     * @return
     */
    @GetMapping("user/save")
    public Object insertUser() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", 1);
        dataMap.put("name", "上海-张三");
        dataMap.put("age", 12);
        dataMap.put("createTime", 1611378102795L);
        return elasticUtils.insert(INDEX_NAME, dataMap);
    }

    @GetMapping("/batchInsert")
    public boolean batchInsert() {
        List<Map<String, Object>> userIndexList = new ArrayList<>();
        Map<String, Object> dataMap1 = new HashMap<>();
        dataMap1.put("id", 2);
        dataMap1.put("name", "北京-李四");
        dataMap1.put("age", 13);
        dataMap1.put("createTime", 1611378102795L);
        Map<String, Object> dataMap2 = new HashMap<>();
        dataMap2.put("id", 3);
        dataMap2.put("name", "深圳-王五");
        dataMap2.put("age", 18);
        dataMap2.put("createTime", System.currentTimeMillis());
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap3.put("id", 4);
        dataMap3.put("name", "杭州-赵六");
        dataMap3.put("age", 19);
        dataMap3.put("createTime", System.currentTimeMillis());
        userIndexList.add(dataMap1);
        userIndexList.add(dataMap2);
        userIndexList.add(dataMap3);
        return elasticUtils.batchInsert(INDEX_NAME, userIndexList);
    }

    @GetMapping("/update")
    public boolean update() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("id", 1);
        dataMap.put("age", 10);
        dataMap.put("updateTime", System.currentTimeMillis());
        return elasticUtils.update(INDEX_NAME, dataMap);
    }
}
