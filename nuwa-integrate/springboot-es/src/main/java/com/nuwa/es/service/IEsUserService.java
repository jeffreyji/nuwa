//package com.nuwa.es.service;
//
//import com.nuwa.es.model.User;
//import org.springframework.data.domain.Page;
//
//import java.util.Iterator;
//import java.util.List;
//
///**
// * @Author jijunhui
// * @Date 2021/6/16 22:20
// * @Version 1.0.0
// * @Description es 用户相关接口
// */
//public interface IEsUserService<User> {
//    void createIndex();
//
//    void deleteIndex(String index);
//
//    User save(User docBean);
//
//    void update(User user);
//
//    void saveAll(List<User> list);
//
//    Iterator<User> findAll();
//
//    Page<User> findByDesc(String desc);
//
//    Page<User> findByFirstName(String firstName);
//
//    Page<User> query(String key);
//}
