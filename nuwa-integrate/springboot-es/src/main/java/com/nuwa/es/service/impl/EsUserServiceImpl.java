//package com.nuwa.es.service.impl;
//
//import com.nuwa.es.model.User;
//import com.nuwa.es.service.IEsUserService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository;
//import org.springframework.stereotype.Service;
//
//import java.util.Iterator;
//import java.util.List;
//
///**
// * @Author jijunhui
// * @Date 2021/6/16 22:23
// * @Version 1.0.0
// * @Description
// */
//@Service
//@Slf4j
//public class EsUserServiceImpl implements IEsUserService<User> {
//    @Autowired
//    private SimpleElasticsearchRepository esUserRepository;
//
//    @Override
//    public void createIndex() {
//        return;
//    }
//
//    @Override
//    public void deleteIndex(String index) {
//
//    }
//
//    @Override
//    public User save(User user) {
//        final User save = (User) esUserRepository.save(user);
//        log.info("es user save success:{}", save);
//        return save;
//    }
//
//    @Override
//    public void update(User user) {
//        return;
//
//    }
//
//    @Override
//    public void saveAll(List<User> list) {
//        final Iterable<User> users = esUserRepository.saveAll(list);
//        log.info("es user save all success:{}", users);
//    }
//
//    @Override
//    public Iterator<User> findAll() {
//        return esUserRepository.findAll().iterator();
//    }
//
//    @Override
//    public Page<User> findByDesc(String desc) {
//        return null;
//    }
//
//    @Override
//    public Page<User> findByFirstName(String firstName) {
//        return null;
//    }
//
//    @Override
//    public Page<User> query(String key) {
//        return null;
//    }
//}
