package com.nuwa.oauth2.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: JeffreyJi
 * @date: 2020/11/14 23:17
 * @version: 1.0
 * @description: 权限控制
 */
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

    @Secured("ROLE_ADMIN,ROLE_USER")
    @RequestMapping("add")
    public String add() {
        return "都可以访问添加页面";
    }

    //    @RolesAllowed("ROLE_ADMIN")
    @RequestMapping("delete")
    @PreAuthorize("hasRole('ADMIN')")
    public String delete() {
        return "只有admin可以访问";
    }

}
