package com.nuwa.oauth2.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: JeffreyJi
 * @date: 2020/11/14 23:17
 * @version: 1.0
 * @description: TODO
 */
@Controller
@Slf4j
public class IndexController {

    @RequestMapping("ok")
    public String ok() {
        return "ok";
    }

    @RequestMapping("login")
    public String login() {
        return "login";
    }

    @RequestMapping("fail")
    public String fail() {
        return "fail";
    }

    @RequestMapping("ad")
    public String ad() {
        return "ad";
    }

    @RequestMapping("limit")
    public String limit() {
        return "auth/limit";
    }


}
