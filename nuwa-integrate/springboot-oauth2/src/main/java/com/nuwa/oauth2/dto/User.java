package com.nuwa.oauth2.dto;

import lombok.Data;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/11/18 16:38
 * @description 用户信息
 */
@Data
public class User {
    private long id;
    private String username;
    private String password;
}
