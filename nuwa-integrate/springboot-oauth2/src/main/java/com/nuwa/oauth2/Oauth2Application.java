package com.nuwa.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: JeffreyJi
 * @date: 2020/11/14 23:14
 * @version: 1.0
 * @description: springboot集成oauth2
 * // 排除默认密码验证 @SpringBootApplication(exclude = SecurityAutoConfiguration.class)
 */

@SpringBootApplication
public class Oauth2Application {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2Application.class, args);
    }

}
