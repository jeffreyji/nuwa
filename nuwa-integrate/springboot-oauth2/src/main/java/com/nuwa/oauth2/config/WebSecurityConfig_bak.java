//package com.nuwa.oauth2.config;
//
//import com.nuwa.oauth2.filter.JwtAuthenticationFilter;
//import com.nuwa.oauth2.filter.JwtLoginFilter;
//import com.nuwa.oauth2.handler.AuthLimitHandler;
//import com.nuwa.oauth2.handler.LoginFailHandler;
//import com.nuwa.oauth2.handler.LoginSuccessHandler;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
///**
// * @author: JeffreyJi
// * @date: 2020/11/15 20:04
// * @version: 1.0
// * @description: 内存方式配置用户
// */
//@Configuration
//@EnableWebSecurity
//@Slf4j
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, jsr250Enabled = true)
//public class WebSecurityConfig_bak extends WebSecurityConfigurerAdapter {
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // 开启跨域共享 跨域伪造请求无效
//        http.cors().and().csrf().disable()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                // 开启认证模式
//                .authorizeRequests().anyRequest().permitAll()
//                .and()
//                // 添加JWT登录拦截器
//                .addFilter(new JwtLoginFilter(authenticationManager()))
//                .addFilter(new JwtAuthenticationFilter(authenticationManager()))
//                .logout().logoutUrl("/logout")
//                .logoutSuccessUrl("/login").permitAll();
//
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN");
//        auth.inMemoryAuthentication().withUser("user").password(passwordEncoder().encode("user")).roles("USER");
//    }
//
//    @Bean
//    public WebMvcConfigurer corsConfigurer() {
//        return new WebMvcConfigurer() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**");
//            }
//        };
//    }
//
//    /**
//     * @Author: JeffreyJi
//     * @Date: 2020/11/15 20:15
//     * @Param: []
//     * @version: 1.0
//     *  注入passwordEncoder bean
//     **/
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        PasswordEncoder encoder = new BCryptPasswordEncoder(16);
//        String result = encoder.encode("admin");
//        log.info("encoder.encode(\"admin\"):{}",result);
//        log.info("password compare:{}",result.equals("admin"));
//
////        encoder =new Argon2PasswordEncoder();
////        result = encoder.encode("admin");
////        log.info("encoder.encode(\"admin\"):{}",result);
////        log.info("password compare:{}",result.equals("admin"));
//        return encoder;
//    }
//
//
//    private void login1(HttpSecurity http) throws Exception {
//        // 配置登陆页面
//        http.formLogin();
//        // 授权配置
//        http.authorizeRequests().anyRequest().fullyAuthenticated();
//    }
//
//    private void login2(HttpSecurity http) throws Exception {
//        // 配置登陆页面
//        http.formLogin().loginPage("/login").permitAll();
//
//        // 自定义配置登陆成功页面
////        http.formLogin().successForwardUrl("/ok");
//        http.formLogin().successHandler(new LoginSuccessHandler());
//        // 用户权限不足处理器
//        http.exceptionHandling().accessDeniedHandler(new AuthLimitHandler());
//        // 配置失败页面
//        http.formLogin().failureForwardUrl("/fail");
//
//        http.logout().permitAll();
//        // 授权配置
//        http.authorizeRequests()
//                // 所有静态页面直接访问
//                .antMatchers("/js/**", "/css/**", "images/**").permitAll()
//                // 所有广告页面直接访问
//                .antMatchers("/ad/**").permitAll()
//                .anyRequest().fullyAuthenticated();
//    }
//
//    private void login3(HttpSecurity http) throws Exception{
//        // 开启跨域共享 跨域伪造请求
//        http.cors().and().csrf().disable();
//        // 开启认证模式
//        http.authorizeRequests().anyRequest().authenticated();
//        // 配置登陆页面
//        http.formLogin().loginPage("/login").permitAll();
////        // 登录配置
////        http.formLogin().usernameParameter("userName").passwordParameter("password").loginProcessingUrl("/login");
//        // 登录失败后处理
//        http.formLogin().failureHandler(new LoginFailHandler());
//        // 登录过期、未登录处理
////        http.exceptionHandling().authenticationEntryPoint(new LoginExpireHandler());
//        // 权限不足的处理
//        http.exceptionHandling().accessDeniedHandler(new AuthLimitHandler());
//        // 登录成功
//        http.formLogin().successHandler(new LoginSuccessHandler());
//    }
//}
