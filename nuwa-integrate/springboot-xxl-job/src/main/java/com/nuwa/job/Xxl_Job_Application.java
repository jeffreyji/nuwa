package com.nuwa.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/11/10 19:19
 * @description xxl-job集成启动类
 */
@SpringBootApplication
public class Xxl_Job_Application {

    public static void main(String[] args) {
        SpringApplication.run(Xxl_Job_Application.class, args);
    }
}
