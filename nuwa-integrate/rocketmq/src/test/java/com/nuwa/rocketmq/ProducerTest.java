package com.nuwa.rocketmq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.nuwa.rocketmq.RocketConstant.NAME_SERVER;
import static com.nuwa.rocketmq.RocketConstant.ORDER_TOPIC;
import static com.nuwa.rocketmq.RocketConstant.PRODUCER_GROUP;
import static com.nuwa.rocketmq.RocketConstant.TAG1;
import static com.nuwa.rocketmq.RocketConstant.TOPIC1;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/11 14:53
 */
@Slf4j
public class ProducerTest {

    private DefaultMQProducer mqProducer;

    @BeforeEach
    public void before() throws Exception {
        // 实例化消息生产者Producer
        mqProducer = new DefaultMQProducer(PRODUCER_GROUP);
        // 设置nameserver的地址
        mqProducer.setNamesrvAddr(NAME_SERVER);
        // 生产者实例启动
        mqProducer.start();
    }

    @AfterEach
    public void after() {
        // 关闭实例
        mqProducer.shutdown();
    }

    /**
     * 同步发送消息 可靠消息 发送短信、邮件等
     *
     * @throws Exception
     */
    @Test
    public void testSync() throws Exception {
        // 发送消息
        for (int i = 0; i < 100; i++) {
            // 创建消息，并指定topic tags 和消息体
            Message msg = new Message();
            msg.setTopic(TOPIC1);
            msg.setTags(TAG1);
            msg.setBody(("hello rocketmq " + i).getBytes(StandardCharsets.UTF_8));
            final SendResult result = mqProducer.send(msg);
            log.info("消息发送结果:{}", result);
        }
    }

    // 异步发送消息
    @Test
    public void testAsync() throws Exception {
        int messageCount = 100;
        final CountDownLatch countDownLatch = new CountDownLatch(messageCount);
        for (int i = 0; i < messageCount; i++) {
            final int index = i;
            Message msg = new Message(TOPIC1, TAG1, "async-key", ("async-msg-" + i).getBytes(StandardCharsets.UTF_8));
            mqProducer.send(msg, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    countDownLatch.countDown();
                    log.info("异步消息{}返回结果:{}", index, sendResult);
                }

                @Override
                public void onException(Throwable throwable) {
                    countDownLatch.countDown();
                    log.error("异步消息{}发送失败:{}", index, throwable.getMessage(), throwable);
                }
            });
        }
        countDownLatch.await(10, TimeUnit.SECONDS);
    }

    /**
     * 单向发送消息 主要发送不是特别关心发送结果的场景，比如日志
     */
    @Test
    public void testOneWay() throws Exception {
        for (int i = 0; i < 100; i++) {
            Message msg = new Message(TOPIC1, TAG1, "oneway-key", ("oneway-msg-" + i).getBytes(StandardCharsets.UTF_8));
            mqProducer.sendOneway(msg);
            log.info("单向消息:{}发送成功！", i);
        }
    }

    /**
     * 发送顺序消息
     *
     * @throws Exception
     */
    @Test
    public void testOrderSend() throws Exception {
        // 模拟生产订单信息
        List<Map<String, String>> orders = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            for (int j = 0; j < 4; j++) {
                Map<String, String> orderMap = new HashMap<>();
                orderMap.put("id", i + "");
                orderMap.put("status", j + "");
                orders.add(orderMap);
            }
        }
        // 标签集合
        String[] tags = new String[]{"TagA", "TagC", "TagD"};

        for (int i = 0; i < orders.size(); i++) {
            Map<String, String> orderMap = orders.get(i);
            String orderId = orderMap.get("id");
            String body = orderId + "," + orderMap.get("status");
            Message msg = new Message(ORDER_TOPIC, tags[i % tags.length], "KEY" + i, body.getBytes(StandardCharsets.UTF_8));
            // 根据订单id按msgqueue顺序发送
            final SendResult result = mqProducer.send(msg, (mqs, msg1, arg) -> {
                Integer id = (Integer) arg;  //根据订单id选择发送queue
                int index = id % mqs.size();
                return mqs.get((int) index);
            }, Integer.parseInt(orders.get(i).get("id")));
            log.info("顺序发送，分区:{},消息offset:{},消息内容:{}",result.getMessageQueue().getQueueId(),result.getQueueOffset(),new String(msg.getBody()));

        }
    }

    /**
     * 发送延时消息
     * @throws Exception
     */
    @Test
    public void testScheduleSend() throws Exception{

        for (int i = 0; i < 20; i++) {
            Message msg = new Message();
            msg.setTopic(TOPIC1);
            msg.setBody(("延时消息:" + i).getBytes(StandardCharsets.UTF_8));
            // 设置延时等级3,这个消息将在10s之后发送(现在只支持固定的几个时间,详看delayTimeLevel)
            msg.setDelayTimeLevel(3);
            mqProducer.send(msg);
        }
    }

    /**
     * 发送带属性的消息
     * @throws Exception
     */
    @Test
    public void testPropertiesSend()throws Exception{
        for (int i=0; i<10; i++){
            Message msg = new Message(TOPIC1,"带属性的消息".getBytes(StandardCharsets.UTF_8));
            msg.putUserProperty("index",i +"");
            mqProducer.send(msg);
        }
    }

}
