package com.nuwa.rocketmq;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/11 14:51
 */
public interface RocketConstant {
    /**
     * 生产组
     */
    String PRODUCER_GROUP = "producer-group-test";
    /**
     * 消费者组
     */
    String CONSUMER_GROUP = "consumer-group-test";
    /**
     * nameserver 服务地址
     */
    String NAME_SERVER = "121.36.80.21:9876";
    /**
     * 主体
     */
    String TOPIC1 = "test-topic1";
    /**
     * 顺序topic
     */
    String ORDER_TOPIC = "test-topic-order";
    /**
     * 标签
     */
    String TAG1 ="test-tag1";
}
