package com.nuwa.rocketmq;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.nuwa.rocketmq.RocketConstant.CONSUMER_GROUP;
import static com.nuwa.rocketmq.RocketConstant.NAME_SERVER;
import static com.nuwa.rocketmq.RocketConstant.ORDER_TOPIC;
import static com.nuwa.rocketmq.RocketConstant.TOPIC1;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/11 16:41
 */
@Slf4j
public class ConsumerTest {

    private DefaultMQPushConsumer mqPushConsumer;

    @BeforeEach
    public void before() throws Exception {
        // 实例化消息消费者
        mqPushConsumer = new DefaultMQPushConsumer(CONSUMER_GROUP);
        // 设置nameserver的地址
        mqPushConsumer.setNamesrvAddr(NAME_SERVER);

    }

    @AfterEach
    public void after() {
        // 关闭实例
        mqPushConsumer.shutdown();
    }

    @Test
    public void testConsumer() throws Exception{
        // topic订阅 1个或者多个
        mqPushConsumer.subscribe(TOPIC1,"*");
        //mqPushConsumer.subscribe(ORDER_TOPIC,"*");
        // 注册回调实现类处理从broker里拉取到的消息
        mqPushConsumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> {
            log.info("本次拉取到的消息:{}条",msgs.size());
            log.info("context.getMessageQueue():{}",context.getMessageQueue());
            log.info("context.getAckIndex():{}",context.getAckIndex());
            msgs.forEach(msg->{
                log.info("消息:{}",new String(msg.getBody()));
            });
            // 标记消息已经被成功消费
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        // 消费者实例启动
        mqPushConsumer.start();
        System.in.read();
    }

    @Test
    public void testOrderConsumer() throws Exception{
        // 设置消费位置
        mqPushConsumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
        // topic订阅 1个或者多个
        mqPushConsumer.subscribe(ORDER_TOPIC,"TagA || TagC || TagD");
        // 注册回调实现类处理从broker里拉取到的消息
        mqPushConsumer.registerMessageListener((MessageListenerOrderly) (msgs, context) -> {
            context.setAutoCommit(true);
            msgs.forEach(msg->{
                log.info("分区:{}，消息内容:{},消息offset:{}",msg.getQueueId(),new String(msg.getBody()),msg.getQueueOffset());
            });
            try {
                // 模拟业务
                TimeUnit.MILLISECONDS.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 标记消息已经被成功消费
            return ConsumeOrderlyStatus.SUCCESS;
        });
        // 消费者实例启动
        mqPushConsumer.start();
        System.in.read();
    }

    @Test
    public void testScheduleConsumer() throws Exception{
        // topic订阅 1个或者多个
        mqPushConsumer.subscribe(TOPIC1,"*");
        // 注册回调实现类处理从broker里拉取到的消息
        mqPushConsumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> {
            msgs.forEach(msg->{
                log.info("消息:{},时间:{}",new String(msg.getBody()),(System.currentTimeMillis() - msg.getBornTimestamp()) + "ms later");
            });
            // 标记消息已经被成功消费
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        // 消费者实例启动
        mqPushConsumer.start();
        System.in.read();
    }

    @Test
    public void testPropertiesConsumer() throws Exception{
        // topic订阅 1个或者多个
        mqPushConsumer.subscribe(TOPIC1, MessageSelector.bySql("index between 3 and 8"));
        // 注册回调实现类处理从broker里拉取到的消息
        mqPushConsumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> {
            msgs.forEach(msg->{
                log.info("消息:{},时间:{},index:{}",new String(msg.getBody()),(System.currentTimeMillis() - msg.getBornTimestamp()) + "ms later",msg.getUserProperty("index"));
            });
            // 标记消息已经被成功消费
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        // 消费者实例启动
        mqPushConsumer.start();
        System.in.read();
    }

}
