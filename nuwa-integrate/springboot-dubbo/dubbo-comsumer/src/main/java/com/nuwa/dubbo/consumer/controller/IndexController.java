package com.nuwa.dubbo.consumer.controller;

import com.nuwa.dubbo.interfaces.AFacadeService;
import com.nuwa.dubbo.interfaces.BFacadeService;
import com.nuwa.dubbo.interfaces.CFacadeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.skywalking.apm.toolkit.trace.Trace;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:48
 * @version: 1.0
 * @description: TODO
 */
@RestController
@Slf4j
public class IndexController {

    @DubboReference
    private AFacadeService aFacadeService;

    @DubboReference
    private BFacadeService bFacadeService;

    @DubboReference
    private CFacadeService cFacadeService;

    @GetMapping("index")
    @Trace
    public String index() {
        String a = aFacadeService.getA();
        log.info("a:{}", a);
        String b = bFacadeService.getB();
        log.info("b:{}", b);
        String c = cFacadeService.getC();
        log.info("c:{}", c);
        return a + b + c;
    }

    @GetMapping("getBtoC")
    public String getBtoC() {
        String btoC = bFacadeService.getBtoC();
        log.info("result:{}", btoC);
        return btoC;
    }

    @GetMapping("getError1")
    public String getError1() {
        String a = aFacadeService.getA();
        String b = bFacadeService.getB();
        int n = 100 / 0;
        String c = cFacadeService.getC();
        return a + b + c;
    }

    @GetMapping("getError2")
    public String getError2() {
        String a = aFacadeService.getA();
        String b = bFacadeService.getB();
        String c = cFacadeService.getC();
        int n = 100 / 0;
        return a + b + c;
    }
}
