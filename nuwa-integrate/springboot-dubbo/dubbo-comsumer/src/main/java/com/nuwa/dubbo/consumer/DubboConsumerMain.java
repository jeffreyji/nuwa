package com.nuwa.dubbo.consumer;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:47
 * @version: 1.0
 * @description: TODO
 */
@SpringBootApplication
@EnableDubbo
public class DubboConsumerMain {
    public static void main(String[] args) {
        SpringApplication.run(DubboConsumerMain.class, args);
    }
}
