package com.nuwa.dubbo.c.service.impl.facade;


import com.nuwa.dubbo.c.service.IUserService;
import com.nuwa.dubbo.interfaces.CFacadeService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:04
 * @version: 1.0
 * @description: TODO
 */
@DubboService
public class CFacadeServiceImpl implements CFacadeService {
    @Autowired
    private IUserService userService;

    @Override
    public String getC() {
        return userService.getById(1).toString();
    }
}
