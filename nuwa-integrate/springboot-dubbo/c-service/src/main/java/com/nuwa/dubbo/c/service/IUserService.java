package com.nuwa.dubbo.c.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuwa.dubbo.c.entity.User;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author jijunhui
 * @since 2020-07-01
 */
public interface IUserService extends IService<User> {
//    List<User> selectPage(IPage<User> page, Integer state);
}
