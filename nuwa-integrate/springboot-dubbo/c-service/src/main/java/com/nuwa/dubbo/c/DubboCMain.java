package com.nuwa.dubbo.c;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:19
 * @version: 1.0
 * @description: TODO
 */
@SpringBootApplication(scanBasePackages = {"com.nuwa"})
@EnableDubbo
public class DubboCMain {
    public static void main(String[] args) {
        SpringApplication.run(DubboCMain.class, args);
    }
}
