package com.nuwa.dubbo.c.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.dubbo.c.dao.UserMapper;
import com.nuwa.dubbo.c.entity.User;
import com.nuwa.dubbo.c.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author jijunhui
 * @since 2020-07-01
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


//    @Override
//    public List<User> selectPage(IPage<User> page, Integer state) {
//        return this.baseMapper.selectPage(page,state);
//    }
}
