package com.nuwa.dubbo.c.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author jijunhui
 * @since 2020-07-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("TB_USER")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableField("ID")
    private Long id;

    /**
     * 姓名
     */
    @TableField("NICK_NAME")
    private String nickName;

    /**
     * 手机号
     */
    @TableField("PHONE")
    private String phone;

    /**
     * 状态 0：男 1：女
     */
    @TableField("SEX")
    private Integer sex;

    /**
     * 状态 0：正常 1：离职
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 注册时间
     */
    @TableField(value = "REGISTER_TIME")
    private Date registerTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 删除标识 0：正常  1：已删除
     */
    @TableLogic
    @TableField(value = "DEL_FLAG", fill = FieldFill.INSERT)
    private Integer delFlag;

}
