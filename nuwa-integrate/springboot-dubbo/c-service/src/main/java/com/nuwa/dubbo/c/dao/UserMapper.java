package com.nuwa.dubbo.c.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuwa.dubbo.c.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author jijunhui
 * @since 2020-07-01
 */
public interface UserMapper extends BaseMapper<User> {

//    @Select("SELECT * FROM tb_user")
//    List<User> selectPage(IPage<User> page, Integer state);


}
