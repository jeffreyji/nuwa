package com.nuwa.dubbo.b;


import com.nuwa.dubbo.interfaces.BFacadeService;
import com.nuwa.dubbo.interfaces.CFacadeService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:04
 * @version: 1.0
 * @description: TODO
 */
@DubboService
public class BService implements BFacadeService {

    @DubboReference
    private CFacadeService cFacadeService;

    @Override
    public String getB() {
        return "返回的是B服务的结果";
    }

    @Override
    public String getBtoC() {
        return cFacadeService.getC();
    }
}
