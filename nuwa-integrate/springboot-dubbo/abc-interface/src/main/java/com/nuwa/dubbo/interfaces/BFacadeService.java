package com.nuwa.dubbo.interfaces;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:05
 * @version: 1.0
 * @description: TODO
 */
public interface BFacadeService {
    String getB();

    /**
     * b 请求c
     *
     * @return
     */
    String getBtoC();
}
