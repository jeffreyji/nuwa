package com.nuwa.dubbo.interfaces;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:05
 * @version: 1.0
 * @description: a服务接口
 */
public interface AFacadeService {
    String getA();
}
