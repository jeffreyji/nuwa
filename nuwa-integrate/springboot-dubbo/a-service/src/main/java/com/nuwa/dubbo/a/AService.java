package com.nuwa.dubbo.a;

import com.nuwa.dubbo.interfaces.AFacadeService;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author: JeffreyJi
 * @date: 2020/12/9 23:04
 * @version: 1.0
 * @description: TODO
 */
@DubboService
public class AService implements AFacadeService {
    @Override
    public String getA() {
        System.out.println("请求到这里了:" + ThreadLocalRandom.current().nextInt());
        return "返回的是A服务的结果";
    }
}
