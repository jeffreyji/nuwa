package com.nuwa.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * kafka消费
 */
@Component
@Slf4j
public class KafkaConsumer {

    @KafkaListener(topics = {"jeffrey"})
    public void listen(ConsumerRecord<?,?> record){
        log.info("获取到的数据:topic:{},key:{},value:{},offset:{}",record.topic(),record.key(),record.value(),record.offset());
    }
}
