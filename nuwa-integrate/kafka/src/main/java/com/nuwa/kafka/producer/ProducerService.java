package com.nuwa.kafka.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.List;

@Component
@Slf4j
public class ProducerService {
    /**
     *
     */
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendNormalMsg(String topic, String msg) {
        kafkaTemplate.send(topic, msg);
        log.info("非事物消息:{}发送成功！", msg);
    }

    public void sendTransactionMsg(String topic, List<String> msgs) {
        kafkaTemplate.executeInTransaction(kafkaOperations -> {
            for (String msg : msgs) {
                if (msg.contains("10")){
                    throw new RuntimeException("消息中包含字符串10");
                }
                kafkaOperations.send(topic,msg);
            }
            return true;
        });
    }
}
