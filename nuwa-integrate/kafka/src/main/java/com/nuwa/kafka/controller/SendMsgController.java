package com.nuwa.kafka.controller;

import com.nuwa.kafka.producer.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("send")
public class SendMsgController {
    @Autowired
    private ProducerService producerService;

    @PostMapping("sendNormalMsg")
    public String sendNormalMsg() {
        producerService.sendNormalMsg("jeffrey", "msg:" + new Random(100).nextInt());
        return "success!";
    }

    @PostMapping("sendTransactionMsg")
    public String sendTransactionMsg(int num){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            list.add("消息" + i);
        }
        producerService.sendTransactionMsg("jeffrey",list);
        return "success";
    }

}
