package com.nuwa.seckill.service.impl;

import com.nuwa.common.cache.annotation.RedissonLock;
import com.nuwa.common.global.expection.ServiceException;
import com.nuwa.seckill.dao.IGoodEntityService;
import com.nuwa.seckill.dao.IOrderEntityService;
import com.nuwa.seckill.entity.Good;
import com.nuwa.seckill.entity.Order;
import com.nuwa.seckill.service.DistributedOrderService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * @author: Jeffrey
 * @date: 2021/12/19 下午11:43
 * @version: 1.0
 * @description: TODO
 */
@Service
@Slf4j
public class DistributedOrderServiceImpl implements DistributedOrderService {

    @Autowired
    private IGoodEntityService goodEntityService;
    @Autowired
    private IOrderEntityService orderEntityService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    @RedissonLock
    public String ms1(Long goodId) {
        final val good = goodEntityService.getById(goodId);
        if (null == good) {
            throw new ServiceException("该商品不存在!");
        }
        if (good.getStoreNum() <= 0) {
            throw new ServiceException("库存不足！");
        }
        // 保存订单信息
        final val order = save(good);
        // 扣减库存
        good.setStoreNum(good.getStoreNum() - 1);
        good.setUpdateTime(LocalDateTime.now());
        goodEntityService.updateById(good);
        log.info("商品:{}口减库存成功!", good.getName());
        return order.getId().toString();
    }

    /**
     * 保存订单信息
     *
     * @param g
     * @return
     */
    private Order save(Good g) {
        Order order = new Order();
        order.setId(System.nanoTime() + new Random(10000).nextInt());
        order.setBuyNum(1);
        order.setBuyerId(1L);
        order.setDeleted(false);
        order.setGoodId(g.getId());
        order.setBuyPrice(g.getSeckillPrice());
        order.setStatus(0);
        order.setTotalPrice(g.getSeckillPrice().multiply(new BigDecimal(order.getBuyNum())));
        orderEntityService.save(order);
        log.info("订单信息保持成功:{}", order);
        return order;
    }
}