package com.nuwa.seckill.service.impl;

import com.nuwa.seckill.constant.SeckillConstant;
import com.nuwa.seckill.service.StandOrderService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: jijunhui
 * @date:2021/12/15 9:50
 */
@Component
@Slf4j
public class OrderTask implements ApplicationRunner {
    @Autowired
    private StandOrderService standOrderService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        while (true) {
            final val id = SeckillConstant.MS_QUEUE.poll();
            if (null != id) {
                try{
                    standOrderService.ms4(id);
                }catch (Exception e){
                    log.error("秒杀错误:{}",e.getMessage(),e);
                }
            } else {
//                log.info("暂时无任务执行，睡眠100ms");
                TimeUnit.MILLISECONDS.sleep(100);
            }
        }
    }
}
