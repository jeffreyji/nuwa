package com.nuwa.seckill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tr_batch_good")
public class BatchGood implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("ID")
    private Long id;

    /**
     * 专题ID
     */
    @TableField("SUBJECT_ID")
    private Long subjectId;

    /**
     * 场次ID
     */
    @TableField("BATCH_ID")
    private Long batchId;

    /**
     * 商品ID
     */
    @TableField("GOOD_ID")
    private Long goodId;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 删除标识 0：正常 1：删除
     */
    @TableField(value = "DELETED", fill = FieldFill.INSERT)
    private Boolean deleted;


}
