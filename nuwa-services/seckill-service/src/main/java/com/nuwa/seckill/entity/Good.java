package com.nuwa.seckill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_good")
public class Good implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("ID")
    private Long id;

    /**
     * 商品名称
     */
    @TableField("NAME")
    private String name;

    /**
     * 图片地址
     */
    @TableField("PHOTO_URI")
    private String photoUri;

    /**
     * 描述信息
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 原价
     */
    @TableField("PRICE")
    private BigDecimal price;

    /**
     * 折扣价格
     */
    @TableField("SECKILL_PRICE")
    private BigDecimal seckillPrice;

    /**
     * 库存数量
     */
    @TableField("STORE_NUM")
    private Integer storeNum;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 删除标识 0：正常 1：删除
     */
    @TableField(value = "DELETED", fill = FieldFill.INSERT)
    private Boolean deleted;


}
