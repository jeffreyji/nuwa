package com.nuwa.seckill.service;

/**
 * @Author jijunhui
 * @Date 2021/12/12 16:06
 * @Version 1.0.0
 * @Description
 */
public interface StandOrderService {
    /**
     * 单机版秒杀-版本1
     * 悲观锁实现 select.. for update; 必须在事务内，否则失效。
     * @param goodId
     * @return
     */
    String ms1(Long goodId);

    /**
     * 单机版秒杀-版本2
     * 乐观锁实现 更新库存用version
     * @param goodId
     * @return
     */
    String ms2(Long goodId);

    /**
     * 单机版秒杀-版本3
     * 加内存锁实现 这里需要注意 锁的释放时间和事务的提交时间的顺序，可能出现超卖
     * @param goodId
     * @return
     */
    String ms3(Long goodId);

    /**
     * 单机版秒杀-版本4
     * 内存队列实现
     * @param goodId
     * @return
     */
    String ms4(Long goodId);
}
