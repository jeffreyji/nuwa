package com.nuwa.seckill.dao.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.seckill.dao.mapper.GoodMapper;
import com.nuwa.seckill.entity.Good;
import com.nuwa.seckill.dao.IGoodEntityService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Service
public class GoodEntityServiceImpl extends ServiceImpl<GoodMapper, Good> implements IGoodEntityService {

    @Autowired
    private GoodMapper goodMapper;
    @Override
    public Good getByIdForUpdate(Long id) {
        return goodMapper.getByIdForUpdate(id);
    }
}
