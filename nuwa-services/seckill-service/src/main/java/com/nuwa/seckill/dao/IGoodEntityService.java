package com.nuwa.seckill.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuwa.seckill.entity.Good;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
public interface IGoodEntityService extends IService<Good> {
    /**
     * 加事务查询
     * @param id
     * @return
     */
    Good getByIdForUpdate(Long id);
}
