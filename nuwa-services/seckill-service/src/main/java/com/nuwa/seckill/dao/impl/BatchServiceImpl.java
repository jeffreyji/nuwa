package com.nuwa.seckill.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.seckill.dao.mapper.BatchMapper;
import com.nuwa.seckill.entity.Batch;
import com.nuwa.seckill.dao.IBatchService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Service
public class BatchServiceImpl extends ServiceImpl<BatchMapper, Batch> implements IBatchService {

}
