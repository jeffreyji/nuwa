package com.nuwa.seckill.constant;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @description: 常量类
 * @author: jijunhui
 * @date:2021/12/15 9:44
 */
public interface SeckillConstant {
    // 同步队列 队列的长度最多100
    Queue<Long> MS_QUEUE = new ArrayBlockingQueue(100);
}
