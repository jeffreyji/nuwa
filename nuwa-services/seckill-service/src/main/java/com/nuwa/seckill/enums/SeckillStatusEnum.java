package com.nuwa.seckill.enums;

/**
 * 秒杀状态
 */
public enum SeckillStatusEnum {

    MUCH(2, "哎呦喂，人也太多了，请稍后！"),
    SUCCESS(1, "秒杀成功"),
    END(0, "秒杀结束"),
    REPEAT_KILL(-1, "重复秒杀"),
    INNER_ERROR(-2, "系统异常"),
    DATE_REWRITE(-3, "数据篡改");

    private int status;
    private String info;

    SeckillStatusEnum(int status, String info) {
        this.status = status;
        this.info = info;
    }

    public int getStatus() {
        return status;
    }


    public String getInfo() {
        return info;
    }


    public static SeckillStatusEnum stateOf(int index) {
        for (SeckillStatusEnum state : values()) {
            if (state.getStatus() == index) {
                return state;
            }
        }
        return null;
    }
}
