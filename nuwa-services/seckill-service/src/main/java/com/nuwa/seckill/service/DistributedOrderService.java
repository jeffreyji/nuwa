package com.nuwa.seckill.service;

/**
 * @author: Jeffrey
 * @date: 2021/12/19 下午11:42
 * @version: 1.0
 * @description: 分布式订单接口
 */
public interface DistributedOrderService {

    /**
     * 分布式秒杀-版本1
     * redis分布式锁实现
     * @param goodId
     * @return
     */
    String ms1(Long goodId);
}
