package com.nuwa.seckill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_batch")
public class Batch implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 专题ID
     */
    @TableField("SUBJECT_ID")
    private Long subjectId;

    /**
     * 开始时间
     */
    @TableField("BEGIN_TIME")
    private LocalDateTime beginTime;

    /**
     * 结束时间
     */
    @TableField("END_TIME")
    private LocalDateTime endTime;

    /**
     * 描述信息
     */
    @TableField("DESCRIBE")
    private String describe;

    /**
     * 状态
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 限购策略
     */
    @TableField("STRATEGY")
    private Integer strategy;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 删除标识 0：正常 1：删除
     */
    @TableField(value = "DELETED", fill = FieldFill.INSERT)
    @TableLogic
    private Boolean deleted;


}
