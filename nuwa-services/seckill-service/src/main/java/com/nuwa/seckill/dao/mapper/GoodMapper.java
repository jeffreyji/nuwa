package com.nuwa.seckill.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuwa.seckill.entity.Good;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
public interface GoodMapper extends BaseMapper<Good> {

    /**
     * 加事务查询
     *
     * @param id
     * @return
     */
    @Select("select * from tb_good where id=#{id} for update")
    Good getByIdForUpdate(@Param("id") Long id);

}
