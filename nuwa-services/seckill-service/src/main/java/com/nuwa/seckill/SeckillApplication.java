package com.nuwa.seckill;

//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/28 20:26
 * @description 秒杀程序入口
 */
//@EnableDiscoveryClient
@EnableOpenApi
//@EnableFeignClients(basePackages = {"com.sinoiov.eimp"})
@SpringBootApplication(scanBasePackages = {"com.nuwa"})
public class SeckillApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeckillApplication.class);
    }
}
