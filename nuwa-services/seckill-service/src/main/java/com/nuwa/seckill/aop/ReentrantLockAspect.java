package com.nuwa.seckill.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/29 17:15
 * @description 秒杀锁aop
 */
@Component
@Aspect
@Order(1) // 约小优先级越高
@Slf4j
public class ReentrantLockAspect {
    /**
     * 思考：为什么不用synchronized
     * service 默认是单例的，并发下lock只有一个实例
     */
    private final static Lock lock = new ReentrantLock(true);//互斥锁 参数默认false，不公平锁

    //Service层切点     用于记录错误日志
    @Pointcut("@annotation(com.nuwa.seckill.annotation.SeckillLock)")
    public void lockAspect() {

    }

    @Around("lockAspect()")
    public Object around(ProceedingJoinPoint joinPoint) {
        Object obj = null;
        lock.lock();
        try {
            obj = joinPoint.proceed();
        } catch (Throwable e) {
           log.error("加锁失败:{}",e.getMessage(),e);
            throw new RuntimeException();
        } finally {
            lock.unlock();
        }
        return obj;
    }
}
