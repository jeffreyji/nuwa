package com.nuwa.seckill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId("ID")
    private Long id;

    /**
     * 商品ID
     */
    @TableField("GOOD_ID")
    private Long goodId;

    /**
     * 购买数量
     */
    @TableField("BUY_NUM")
    private Integer buyNum;

    /**
     * 购买价格
     */
    @TableField("BUY_PRICE")
    private BigDecimal buyPrice;

    /**
     * 总共金额
     */
    @TableField("TOTAL_PRICE")
    private BigDecimal totalPrice;

    /**
     * 订单状态
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 下单时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATE_TIME", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 购买人ID
     */
    @TableField("BUYER_ID")
    private Long buyerId;

    /**
     * 删除标识 0：正常 1：删除
     */
    @TableField(value = "DELETED", fill = FieldFill.INSERT)
    private Boolean deleted;


}
