package com.nuwa.seckill.controller;


import com.nuwa.common.global.dto.GlobalResponseDto;
import com.nuwa.seckill.service.DistributedOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@RestController
@RequestMapping("/distribute/order/")
@Slf4j
public class DistributedOrderController {
    @Autowired
    private DistributedOrderService distributeOrderService;

    /**
     * 秒杀第一个版本
     *
     * @return
     */
    @PostMapping("ms1")
    public GlobalResponseDto ms1(@RequestParam("goodId") Long goodId) {
        return GlobalResponseDto.success(distributeOrderService.ms1(goodId));
    }


}
