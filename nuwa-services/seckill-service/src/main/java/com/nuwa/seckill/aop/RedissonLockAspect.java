package com.nuwa.seckill.aop;

import com.nuwa.common.cache.facade.RedissonDistributedLock;
import com.nuwa.common.global.enums.ErrorCodeEnum;
import com.nuwa.common.global.expection.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author: Jeffrey
 * @date: 2021/12/20 下午9:59
 * @version: 1.0
 * @description: 分布式锁
 */
@Aspect
@Component
@Slf4j
@Order(1)
public class RedissonLockAspect {
    public static final int WAIT_GET_LOCK_TIME = 3000;

    public static final int WAIT_RELEASE_LOCK_TIME = 5000;

    @Autowired
    private RedissonDistributedLock iRedissonDistributedLock;

    /**
     * 切点
     */
    @Pointcut("@annotation(com.nuwa.common.cache.annotation.RedissonLock)")
    public void redissonLockPoint() {
    }

    /**
     * @param point 代表当前正在运行的方法
     * @return Object 结果
     * @throws Throwable e
     */
    @Around("redissonLockPoint()")
    public Object checkLock(ProceedingJoinPoint point) throws Throwable {
        // 当前线程名称
        String threadName = Thread.currentThread().getName();
        // 获取当前类名
        Object className = point.getTarget().getClass().getName();
        // 拦截的方法名称
        String methodName = point.getSignature().getName();
        log.info("当前线程:{}------进入分布式锁AOP------", threadName);
        //生成分布式锁:服务名+类名+方法名
        String lockRedisKey = className + ":" + methodName;
        log.info("当前线程：{}，分布式锁的key：{}", threadName, lockRedisKey);
        //获取锁3000等到获取锁的时间,leaseTime 获取锁后持有时间时间,单位 MILLISECONDS：毫秒
        if (iRedissonDistributedLock.tryLock(lockRedisKey, WAIT_GET_LOCK_TIME, WAIT_RELEASE_LOCK_TIME, TimeUnit.MILLISECONDS)) {
            try {
                log.info("当前线程：{}，获取锁成功", threadName);
                return point.proceed();
            } catch (Throwable throwable) {
                throw new Exception(throwable);
            } finally {
                if (iRedissonDistributedLock.isLocked(lockRedisKey)) {
                    log.info("当前线程：{}, key：{}，对应的锁被线程持有", threadName, lockRedisKey);
                    if (iRedissonDistributedLock.isHeldByCurrentThread(lockRedisKey)) {
                        log.info("当前线程：{}，key：{}，保持锁定", threadName, lockRedisKey);
                        iRedissonDistributedLock.unlock(lockRedisKey);
                        log.info("当前线程：{}，key：{}，释放锁", threadName, lockRedisKey);
                    }
                }
            }
        } else {
            log.warn("当前线程：{}，获取锁失败", threadName);
            //这里return可以根据自己业务改造，我这里是返回“系统繁忙，请稍后再试！”
            throw new ServiceException(ErrorCodeEnum.SYSTEM_ERROR);
//            return Result.error(AppErrorCodeEnum.SYSTEM_BUSY.getMessage());
        }
    }
}
