package com.nuwa.seckill.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.seckill.dao.mapper.SubjectMapper;
import com.nuwa.seckill.entity.Subject;
import com.nuwa.seckill.dao.ISubjectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {

}
