package com.nuwa.seckill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.nuwa.common.db.handler.TransactionCommitHandler;
import com.nuwa.common.global.expection.ServiceException;
import com.nuwa.seckill.annotation.SeckillLock;
import com.nuwa.seckill.dao.IGoodEntityService;
import com.nuwa.seckill.dao.IOrderEntityService;
import com.nuwa.seckill.entity.Good;
import com.nuwa.seckill.entity.Order;
import com.nuwa.seckill.service.StandOrderService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * @Author jijunhui
 * @Date 2021/12/12 16:06
 * @Version 1.0.0
 * @Description
 */
@Service
@Slf4j
public class StandOrderServiceImpl implements StandOrderService {
    @Autowired
    private IGoodEntityService goodEntityService;
    @Autowired
    private IOrderEntityService orderEntityService;
    @Autowired
    private TransactionCommitHandler transactionCommitHandler;

    /**
     * 1. 检查库存
     * 2. 保存订单信息
     * 3. 减库存
     *
     * @param goodId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String ms1(Long goodId) {
        // 根据goodId查询是否还有库存
        // 这里加上for update 相当于并行执行了 不会超卖
        //final val good = goodEntityService.getByIdForUpdate(goodId);
        final val good = goodEntityService.getById(goodId);
        if (null == good) {
            throw new ServiceException("该商品不存在!");
        }
        if (good.getStoreNum() <= 0) {
            throw new ServiceException("库存不足！");
        }
        // 保存订单信息
        final val order = save(good);
        // 扣减库存
        good.setStoreNum(good.getStoreNum() - 1);
        good.setUpdateTime(LocalDateTime.now());
        goodEntityService.updateById(good);
        log.info("商品:{}口减库存成功!", good.getName());
        return order.getId().toString();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String ms2(Long goodId) {
        // 先更新库存 利用行锁同步
        final val goodUpdate = Wrappers.lambdaUpdate(Good.class);
        goodUpdate.setSql("STORE_NUM = STORE_NUM -1");
        goodUpdate.eq(Good::getId, goodId).gt(Good::getStoreNum, 0);
        final val updateFlag = goodEntityService.update(goodUpdate);
        if (updateFlag) {
            final val good = goodEntityService.getById(goodId);
            if (null == good) {
                throw new ServiceException("该商品不存在!");
            }
            if (good.getStoreNum() < 0) {
                throw new ServiceException("库存不足！");
            }
            // 保存订单信息
            final val order = save(good);
            return order.getId().toString();
        } else {
            throw new ServiceException("更新失败,版本不一致！");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @SeckillLock
    public String ms3(Long goodId) {
        final val good = goodEntityService.getById(goodId);
        if (null == good) {
            throw new ServiceException("该商品不存在!");
        }
        if (good.getStoreNum() <= 0) {
            throw new ServiceException("库存不足！");
        }
        // 保存订单信息
        final val order = save(good);
        // 扣减库存
        good.setStoreNum(good.getStoreNum() - 1);
        good.setUpdateTime(LocalDateTime.now());
        goodEntityService.updateById(good);
        log.info("商品:{}口减库存成功!", good.getName());
        return order.getId().toString();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String ms4(Long goodId) {
        final val good = goodEntityService.getById(goodId);
        if (null == good) {
            throw new ServiceException("该商品不存在!");
        }
        if (good.getStoreNum() <= 0) {
            throw new ServiceException("库存不足！");
        }
        // 保存订单信息
        final val order = save(good);
        // 扣减库存
        good.setStoreNum(good.getStoreNum() - 1);
        good.setUpdateTime(LocalDateTime.now());
        goodEntityService.updateById(good);
        log.info("商品:{}口减库存成功!", good.getName());
        return order.getId().toString();
    }


    /**
     * 保存订单信息
     *
     * @param g
     * @return
     */
    private Order save(Good g) {
        Order order = new Order();
        order.setId(System.nanoTime() + new Random(10000).nextInt());
        order.setBuyNum(1);
        order.setBuyerId(1L);
        order.setDeleted(false);
        order.setGoodId(g.getId());
        order.setBuyPrice(g.getSeckillPrice());
        order.setStatus(0);
        order.setTotalPrice(g.getSeckillPrice().multiply(new BigDecimal(order.getBuyNum())));
        orderEntityService.save(order);
        log.info("订单信息保持成功:{}", order);
        return order;
    }
}
