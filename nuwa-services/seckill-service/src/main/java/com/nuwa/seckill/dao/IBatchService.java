package com.nuwa.seckill.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuwa.seckill.entity.Batch;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
public interface IBatchService extends IService<Batch> {

}
