package com.nuwa.seckill.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nuwa.seckill.entity.Batch;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
public interface BatchMapper extends BaseMapper<Batch> {

}
