package com.nuwa.seckill.controller;


import com.nuwa.common.global.dto.GlobalResponseDto;
import com.nuwa.seckill.constant.SeckillConstant;
import com.nuwa.seckill.service.StandOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class StandOrderController {
    @Autowired
    private StandOrderService standOrderService;

    /**
     * 秒杀第一个版本
     *
     * @return
     */
    @PostMapping("ms1")
    public GlobalResponseDto ms1(@RequestParam("goodId") Long goodId) {
        return GlobalResponseDto.success(standOrderService.ms1(goodId));
    }

    /**
     * 秒杀第二个版本
     *
     * @return
     */
    @PostMapping("ms2")
    public GlobalResponseDto ms2(@RequestParam("goodId") Long goodId) {
        return GlobalResponseDto.success(standOrderService.ms2(goodId));
    }

    /**
     * 秒杀第3个版本
     *
     * @return
     */
    @PostMapping("ms3")
    public GlobalResponseDto ms3(@RequestParam("goodId") Long goodId) {
        return GlobalResponseDto.success(standOrderService.ms3(goodId));
    }

    /**
     * 秒杀第3个版本
     *
     * @return
     */
    @PostMapping("ms4")
    public GlobalResponseDto ms4(@RequestParam("goodId") Long goodId) {
        // 加入队列
        boolean f = SeckillConstant.MS_QUEUE.add(goodId);
        if (f) {
            return GlobalResponseDto.success();
        }
        log.warn("线程:{}请求频繁请重试!",Thread.currentThread().getName());
        return GlobalResponseDto.fail("请求频繁请重试!");
    }

}
