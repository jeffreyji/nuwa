package com.nuwa.seckill.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuwa.seckill.dao.mapper.BatchGoodMapper;
import com.nuwa.seckill.entity.BatchGood;
import com.nuwa.seckill.dao.IBatchGoodService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jijunhui
 * @since 2021-03-17
 */
@Service
public class BatchGoodServiceImpl extends ServiceImpl<BatchGoodMapper, BatchGood> implements IBatchGoodService {

}
